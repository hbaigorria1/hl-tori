<?php
/*******************************************************************************
********************************************************************************
**************************** TRADUCCION AL ESPAÑOL *****************************
********************************************************************************
********************************************************************************/
$lang['leng_pt'] = 'Portugués';
$lang['leng_es'] = 'Español';

$lang['leng_home'] = 'Inicio';

$lang['leng_somos'] = 'AHORA SOMOS';

$lang['lbl_menu_contacto'] = 'Contacto';
$lang['lbl_tlt_config'] = 'Configuración avanzada';

$lang['lbl_tlt_compon'] = 'Componentes para Optimizar tu Quirófano';
$lang['lbl_tlt_pads'] = 'Pads para Optimizar tu Quirófano';

$lang['lbl_form_contacto'] = 'https://go.pardot.com/l/8232/2022-03-09/bz74pt';

$lang['lbl_ver_mas'] = 'Ver Más';
$lang['lbl_mas_info'] = 'Más información';
$lang['lbl_obtener'] = 'Obtener ahora';

$lang['lbl_cantidad'] = 'CANTIDAD';

$lang['lbl_tipos_procedimientos'] = 'Tipos de Procedimientos';
$lang['lbl_tipos_procedimientos_desc'] = 'TORI te recomendará los equipos y componentes esenciales para optimizar cada procedimiento quirúrgico.';
$lang['lbl_tipos_quirofanos'] = 'Tipos de Quirófanos';
$lang['lbl_tipos_quirofanos_desc'] = 'Selecciona el tipo de sala que deseas evaluar y TORI te brindará la solución quirúrgica que mejor responda a tu necesidad.';
$lang['lbl_soluciones_title'] = 'Soluciones Recomendadas';

$lang['lbl_componentes'] = 'COMPONENTES';
$lang['lbl_componentes_min'] = 'Componentes';
$lang['lbl_pads'] = 'PADS';
$lang['lbl_pads_min'] = 'Pads';
$lang['lbl_solicitar_mas_info'] = 'Solicitar más información';
$lang['lbl_posicion'] = 'Ver posicionamiento';

$lang['lbl_descargar'] = 'Descargar';
$lang['lbl_video_micro'] = 'Ver video Microclima';

$lang['lbl_superficie'] = 'Superficies';
$lang['lbl_procedimientos'] = 'Procedimientos';

$lang['lbl_config_avanzada_01'] = 'Selecciona procedimiento';
$lang['lbl_config_avanzada_02'] = 'Selecciona quirofano';
$lang['lbl_config_avanzada_03'] = 'Selecciona producto';
$lang['lbl_config_avanzada_04'] = 'Ver Configuración';


$lang['lbl_probabilidades'] = 'Ver probabilidades de LPP';
$lang['lbl_versuperficies'] = 'Ver superficies';
$lang['lbl_versuperficies_todas'] = 'Ver todas las superficies';

$lang['lbl_estaticas'] = 'Estáticas';
$lang['lbl_mixta'] = 'Mixtas';
$lang['lbl_dinamica'] = 'Dinámicas';

$lang['lbl_niveles'] = 'Niveles de Riesgo';

$lang['lbl_nivel_01'] = 'BAJO';
$lang['lbl_nivel_02'] = 'BAJO A MODERADO';
$lang['lbl_nivel_03'] = 'MODERADO';
$lang['lbl_nivel_04'] = 'ALTO';
$lang['lbl_nivel_05'] = 'MUY ALTO';

$lang['lbl_piel_01'] = 'Piel con lesión';
$lang['lbl_piel_02'] = 'Piel intacta';

$lang['lbl_riesgo_01'] = 'RIESGO Bajo';
$lang['lbl_riesgo_01_desc'] = 'Recomendamos superficies estáticas de alta calidad y de contenido variable.';

$lang['lbl_riesgo_02'] = 'RIESGO Medio';
$lang['lbl_riesgo_02_desc'] = 'Sugerimos superficies que sean dinámicas con una presión alternante o de presión constante';

$lang['lbl_riesgo_03'] = 'RIESGO Alto';
$lang['lbl_riesgo_03_desc'] = 'Sugerimos superficies que sean dinámicas con una presión alternante o de presión constante';
$lang['lbl_riesgos_txt'] = 'Superficie especializada indicada para el manejo de la presión:';

$lang['menu_nivel'] = 'Niveles';
$lang['menu_clasificacion'] = 'Asistencia para Superficies';

$lang['txt_sup_camas_descr'] = 'Elige la superficie perfecta de acuerdo al Nivel de Riesgo';

$lang['txt_niveles_titulo'] = 'Clasificaciones de Riesgo por Nivel';

/** LEVELES **/
$lang['lbl_bajo'] = 'BAJO';
$lang['lbl_medio'] = 'MEDIO';
$lang['lbl_alto'] = 'ALTO';

$lang['lbl_nombre_link_bajo'] = 'Ver superficies de riesgo bajo';
$lang['lbl_nombre_link_medio'] = 'Ver superficies de riesgo medio';
$lang['lbl_nombre_link_alto'] = 'Ver superficies de riesgo alto';

$lang['txt_t_01'] = 'RIESGO BAJO';
$lang['txt_d_01'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Para personas con un riesgo menor de desarrollar lesiones por presión, recomendamos utilizar superficies estáticas de alta calidad y de contenido variable como lo son el gel, la espuma y las viscoelásticas.</p>
	<a href="'.base_url().'asistencia/superficies?nivel=bajo" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Ver superficies de riesgo bajo</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';
$lang['txt_t_02'] = 'RIESGO MEDIO';
$lang['txt_d_02'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Un paciente con un riesgo medio requiere de más cuidados y por consiguiente, de superficies que sean dinámicas con una presión alternante y, en el caso de que no sea posible este último tipo de superficie, se recomienda una de presión constante.</p>
	<a href="'.base_url().'asistencia/superficies?nivel=medio" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Ver superficies de riesgo medio</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';

$lang['txt_t_03'] = 'RIESGO ALTO';
$lang['txt_d_03'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Un paciente con un riesgo alto requiere de más cuidados y por consiguiente, de superficies que sean dinámicas con una presión alternante y, en el caso de que no sea posible este último tipo de superficie, se recomienda una de presión constante.</p>
	<a href="'.base_url().'asistencia/superficies?nivel=alto" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Ver superficies de riesgo alto</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';

/** ESCALA BRADEN **/

$lang['braden_titulo'] = 'Selecciona la variable correspondiente al paciente en cada categoría, para obtener como resultado el Nivel de Riesgo de desarrollar LPP.';

$lang['braden_titulo_02'] = 'ESCALA BRADEN';

$lang['braden_columna_titulo_01'] = 'PERCEPCIÓN SENSORIAL';
$lang['braden_columna_01_item_01'] = '1. Completamente limitada';
$lang['braden_columna_01_item_02'] = '2. Muy limitada';
$lang['braden_columna_01_item_03'] = '3. Ligeramente limitada';
$lang['braden_columna_01_item_04'] = '4. Sin limitaciones';

$lang['braden_columna_titulo_02'] = 'EXPOSICIÓN A LA HUMEDAD';
$lang['braden_columna_02_item_01'] = '1. Constantemente húmeda';
$lang['braden_columna_02_item_02'] = '2. A menudo húmeda';
$lang['braden_columna_02_item_03'] = '3. Ocasionalmente húmeda';
$lang['braden_columna_02_item_04'] = '4. Raramente húmeda';

$lang['braden_columna_titulo_03'] = 'ACTIVIDAD';
$lang['braden_columna_03_item_01'] = '1. Encamado';
$lang['braden_columna_03_item_02'] = '2. En silla';
$lang['braden_columna_03_item_03'] = '3. Deambula ocasionalmente';
$lang['braden_columna_03_item_04'] = '4. Deambula frecuentemente';

$lang['braden_columna_titulo_04'] = 'MOVILIDAD';
$lang['braden_columna_04_item_01'] = '1. Completamente inmóvil';
$lang['braden_columna_04_item_02'] = '2. Muy limitada';
$lang['braden_columna_04_item_03'] = '3. Ligeramente limitada';
$lang['braden_columna_04_item_04'] = '4. Sin limitaciones';

$lang['braden_columna_titulo_05'] = 'NUTRICIÓN';
$lang['braden_columna_05_item_01'] = '1. Muy pobre';
$lang['braden_columna_05_item_02'] = '2. Probablemente adecuada';
$lang['braden_columna_05_item_03'] = '3. Adecuada';
$lang['braden_columna_05_item_04'] = '4. Excelente';

$lang['braden_columna_titulo_06'] = 'ROCE Y PELIGRO DE LESIONES';
$lang['braden_columna_06_item_01'] = '1. Problema';
$lang['braden_columna_06_item_02'] = '2. Problema potencial';
$lang['braden_columna_06_item_03'] = '3. No existe problema';

$lang['braden_nivel'] = 'Nivel de riesgo:';
$lang['braden_desc'] = 'Puntos de corte: ≤16 riesgo bajo, ≤14 riesgo medio y ≤12 riesgo alto';

$lang['probabilidades_titulo'] = 'Probabilidad de desarrollar una LPP según la superficie de apoyo empleada:';
$lang['probabilidades_item_01'] = '21.8% <small>SUPERFICIE DE ESPUMA ESTÁNDAR</small>';
$lang['probabilidades_item_02'] = '8.9% <small>SUPERFICIE DE BAJA PRESIÓN CONSTANTE</small>';
$lang['probabilidades_item_03'] = '6.8% <small>SUPERFICIE DE PRESIÓN ALTERNANTE</small>';

$lang['algoritmo_home_titulo'] = 'Algoritmo de Asistencia para Superficies';
$lang['algoritmo_home_desc'] = 'basado en el Consenso y la Evidencia';
$lang['algoritmo_home_btn'] = 'Comenzar';
$lang['algoritmo_home_modal'] = 'Superficies especiales para el manejo de la presión (SEMP)<br><br>
		Según el documento XIII del GNEAUPP, las superficies de apoyo (SA) o superficies especiales para el manejo de la presión (SEMP) se definen como la superficie o dispositivo especializado, cuya configuración física y/o estructural permite la redistribución de la presión, así como otras funciones terapéuticas añadidas para el manejo de las cargas tisulares, fricción, cizalla y/o microclima, y que abarca el cuerpo de un individuo o una parte del mismo, según las diferentes posturas funcionales posibles.';

$lang['algoritmo_asistencia_titulo'] = '
		<h3 style="color:#fff">Presencia de riesgo de <div class="item-hints">
		  <div class="hint" data-position="4"><!-- is-hint -->
		    lesiones
		    <div class="hint-content do--split-children">
		      <p>Además de las superficies especiales, el documento de GNEAUPP no. XI 2008, ofrece una serie de cuidados a tener en cuenta como: la valoración de la piel, la movilización, incentivar el autocuidado del paciente, valorar el estado nutricional, realizar cambios posturales, aplicar protección local y vigilar la hidratación e higiene.</p>
		    </div>
		  </div>
		</div> por presión</h3>';

$lang['algoritmo_asistencia_bajada'] = 'Selecciona la variable correspondiente';
$lang['algoritmo_pin'] = 'Piel intacta';
$lang['algoritmo_pin_01'] = 'S&iacute;, est&aacute; en riesgo';
$lang['algoritmo_pin_02'] = 'No est&aacute; en riesgo';

$lang['algoritmo_pcl'] = 'Piel con lesión';

$lang['en_riesgo'] = 'En riesgo';

$lang['algoritmo_piel_titulo'] = '<h3 style="color:#fff;font-size: 40px;">Evaluación del riesgo de
				<div class="item-hints">
				  <div class="hint" data-position="4"><!-- is-hint -->
				    lesiones
				    <div class="hint-content do--split-children">
				      <p>Además de las superficies especiales, el documento de GNEAUPP no. XI 2008, ofrece una serie de cuidados a tener en cuenta como: la valoración de la piel, la movilización, incentivar el autocuidado del paciente, valorar el estado nutricional, realizar cambios posturales, aplicar protección local y vigilar la hidratación e higiene.</p>
				    </div>
				  </div>
				</div>
			 por presión</h3>';

$lang['algoritmo_piel_bajada'] = 'Selecciona el nivel de riesgo según la escala Braden';

$lang['algoritmo_piel_btn_braden'] = 'Ver escala Braden';

$lang['algoritmo_piel_riesgo_01'] = 'RIESGO BAJO';
$lang['algoritmo_piel_riesgo_desc_01'] = '<strong>Mayor de 15 puntos</strong> Escala Braden';

$lang['algoritmo_piel_riesgo_02'] = 'RIESGO MEDIO';
$lang['algoritmo_piel_riesgo_desc_02'] = '<strong>De 13 a 14 puntos</strong> Escala Braden';

$lang['algoritmo_piel_riesgo_03'] = 'RIESGO ALTO';
$lang['algoritmo_piel_riesgo_desc_03'] = '<strong>Menor de 12 puntos</strong> Escala Braden';

$lang['disclamer'] = 'Para más información consulta a tu especialista.<br>Las restricciones de configuración se aplican de acuerdo a los registros de cada país';

$lang['hlbax'] = 'Hillrom es parte de Baxter';


$lang['frase_01'] = 'Sabias que, en promedio, los hospitales tienen 20 quirófanos generales en los EE. UU., 6-7 en Latinoamérica y 12-14 en Europa';
$lang['frase_02'] = 'Dos tercios de los hospitales tienen quirófanos híbridos, entre 2 y 6 por hospital. Los quirófanos robóticos son menos frecuentes en LATAM';
$lang['frase_03'] = 'Los quirófanos generales se renuevan normalmente cada 5 años en EE. UU., Latinoamérica y Reino Unido';
$lang['frase_04'] = 'La calidad, la facilidad de uso, el impacto en la eficiencia y seguridad del flujo de trabajo y la relación calidad-precio son importantes a la hora de elegir productos.';
$lang['frase_05'] = 'Al comprar soluciones del quirófano, la calidad ocupa el primer lugar, seguida de la seguridad y luego la relación calidad-precio.';
$lang['frase_06'] = 'La mayoría de los hospitales tienen quirófanos generales donde los cirujanos y los directores de quirófano pasan la mayor parte de su tiempo.';
$lang['frase_07'] = 'Más de la mitad de los hospitales tienen quirófanos híbridos; los quirófanos robóticos son menos frecuentes.';
$lang['frase_08'] = 'La información del producto es de gran interés para los hospitales que buscan comprar nuevos equipos quirúrgicos.';

$lang['TL3000_01'] = "Variantes de cabezales";
$lang['TL3000_02'] = "Intensidad luminosa Ec a 1 m (lx)";
$lang['TL3000_03'] = "Diámetro de campo variable en función de la distancia (cm)";
$lang['TL3000_04'] = "Diámetro de campo (d10) a 1 m (cm)";
$lang['TL3000_05'] = "Temperatura de color (K)";
$lang['TL3000_06'] = "Indice de reproducción cromática (Ra)";
$lang['TL3000_07'] = "Vida media de los diodos LED’s (h)";
$lang['TL3000_08'] = "Adaptive Light Control (ALC)";
$lang['TL3000_09'] = "IPreparación para cámara";
$lang['TL3000_10'] = "Los valores están sujetos a una tolerancia general de la industria de ± 10%.";

// Table 01
$lang['TRUPORT_01_01'] = "ESTÁNDAR";
$lang['TRUPORT_01_02'] = "VENTAJAS ADIC. CON RIEL MPC";
$lang['TRUPORT_01_03'] = "Sistema de brazos";
$lang['TRUPORT_01_04'] = "Alcance";
$lang['TRUPORT_01_05'] = "Sistema de un solo brazo: hasta 1300 mm<br>Sistema de dos brazos: hasta 3200 mm";
$lang['TRUPORT_01_06'] = "Sistema de frenos";
$lang['TRUPORT_01_07'] = "Freno de fricción<br>Freno neumático";
$lang['TRUPORT_01_08'] = "Freno electroneumático<br>Freno electromagnético[1]";
$lang['TRUPORT_01_09'] = "Fijación de techo";
$lang['TRUPORT_01_10'] = "Separación al techo falso de hasta 2500 mm<br>Antisísmico";
$lang['TRUPORT_01_11'] = "Brazo de altura regulable";
$lang['TRUPORT_01_12'] = "Cabezal de TruPort";
$lang['TRUPORT_01_13'] = "Medidas";
$lang['TRUPORT_01_14'] = "Integración de un dispositivo de elevación motorizado";
$lang['TRUPORT_01_15'] = "Longitudes";
$lang['TRUPORT_01_16'] = "Hasta 1750 mm";
$lang['TRUPORT_01_17'] = "[1] Específico de país";

// Table 02
$lang['TRUPORT_02_01'] = "Módulos de suministro";
$lang['TRUPORT_02_02'] = "Modificación datos";
$lang['TRUPORT_02_03'] = "Modificación gases";
$lang['TRUPORT_02_04'] = "Reorganización del módulo de corriente eléctrica";
$lang['TRUPORT_02_05'] = "Componentes del entorno de trabajo";
$lang['TRUPORT_02_06'] = "Montaje sin herramientas con mecanismo de clic";
$lang['TRUPORT_02_07'] = "Componentes electrónicos";
$lang['TRUPORT_02_08'] = "AmbientLine";
$lang['TRUPORT_02_09'] = "AmbientLine Floor, AmbientLine Ceiling";
$lang['TRUPORT_02_10'] = "AmbientLine Desk, AmbientLine Floor, AmbientLine Ceiling";

// Table 03
$lang['TRUPORT_03_01'] = "Bandejas y cajones";
$lang['TRUPORT_03_02'] = "Capacidad de carga";
$lang['TRUPORT_03_03'] = "Bandeja de carga mediana: 50 kg<br>Bandeja de carga pesada: 80 kg";
$lang['TRUPORT_03_04'] = "Función de mando en la bandeja";
$lang['TRUPORT_03_05'] = "Para bandeja de carga mediana";
$lang['TRUPORT_03_06'] = "Para bandeja de carga pesada";
$lang['TRUPORT_03_07'] = "Dimensiones de las bandejas (A x F)";
$lang['TRUPORT_03_08'] = "Bandeja de carga mediana:<br>
            530, 630 x 480 mm<br>
            Bandeja de carga pesada:<br>
            430[2], 530, 630, 730 x 480 mm<br>
            Kit prolongador (fondo) de 70 mm disponible";
$lang['TRUPORT_03_09'] = "Bandeja de carga mediana:<br>
            530, 630 x 480 mm<br>
            Bandeja de carga pesada:<br>
            430[2], 530, 630, 730 x 480 mm<br>
            Kit prolongador (fondo) de 70 mm disponible";
$lang['TRUPORT_03_10'] = "Número de cajones por bandeja";
$lang['TRUPORT_03_11'] = "Bandeja de carga mediana: max. 2<br>Bandeja de carga pesada: max. 4";
$lang['TRUPORT_03_12'] = "[2] 430 mm de ancho, también disponible con fondo de 260 mm.";

$lang['PST300_01'] = "Dimensiones del tablero de la mesa";
$lang['PST300_02'] = "Peso neto de la mesa";
$lang['PST300_03'] = "Ajuste de altura";
$lang['PST300_04'] = "Trendelenburg/Trendelenburg inverso";
$lang['PST300_05'] = "Inclinación";
$lang['PST300_06'] = "Desplazamiento longitudinal";
$lang['PST300_07'] = '34 cm (±1cm)/13,4" (±0,4") lateral de cabeza; 19,5 cm (7,7")/lateral de pie 14,5 cm (5,7")';
$lang['PST300_08'] = "Rango de ajuste de la sección dorsal";
$lang['PST300_09'] = "Rango de ajuste de la sección de pierna";
$lang['PST300_10'] = "Posicionamiento cero";
$lang['PST300_11'] = "Nivelación motorizada de todas las secciones";
$lang['PST300_12'] = "Capacidad de carga máxima";
$lang['PST300_13'] = "Accesorios compatibles hacia externo inferior";
$lang['PST300_14'] = "Accesorios compatibles hacia externo superior";

//Table 01
$lang['TS7500_01_01'] = "Columnas de mesa";
$lang['TS7500_01_02'] = "Columna de mesa SC";
$lang['TS7500_01_03'] = "Columna de mesa FC";
$lang['TS7500_01_04'] = "Columna de mesa SMC, Hybrid MC<small>2</small>";
$lang['TS7500_01_05'] = "Columna de mesa Hybrid SC, FC";

$lang['TS7500_01_06'] = "Rotación";
$lang['TS7500_01_tope'] = "sin tope";
$lang['TS7500_01_ctope'] = "con tope";
$lang['TS7500_01_07'] = "Altura de la columna";
$lang['TS7500_01_08'] = "Altura mín. incl. tablero de mesa universal H";
$lang['TS7500_01_09'] = "Palanca";
$lang['TS7500_01_10'] = "Sección de la columna (L x A)";
$lang['TS7500_01_11'] = "Trend./ inclinación";
$lang['TS7500_01_12'] = "Altura del tablero base";
$lang['TS7500_01_13'] = "Carga máx. <small>1</small>";
$lang['TS7500_01_14'] = "[1] Con limitaciones según la posición del paciente.<br>[2] La palanca de limpieza para elevar la columna móvil permite la limpieza.";

//Table 02
$lang['TS7500_02_01'] = "Tableros de mesa universales [3-5]";
$lang['TS7500_02_02'] = "Longitud";
$lang['TS7500_02_03'] = "Anchura en rieles estándar";
$lang['TS7500_02_04'] = "Rango de ajuste de la sección de piernas";
$lang['TS7500_02_05'] = "Rango de ajuste de la sección dorsal superior";
$lang['TS7500_02_06'] = "Rango de ajuste de la sección dorsal inferior";
$lang['TS7500_02_07'] = "Trendelenburg";
$lang['TS7500_02_08'] = "Desplazamiento longitudinal";
$lang['TS7500_02_09'] = "Desplazamiento lateral";
$lang['TS7500_02_10'] = "[3] Dispositivos de mando disponibles: a distancia con cable y cargador para mando a distancia inalámbrico (móvil y en montaje de pared).<br>[4] Tableros H disponibles con colchonetas Balanced, CFL y ST26 disponibles con colchonetas Balanced o Comfort Plus.<br>[5] Los tableros para mesa universales también están disponibles en la variante lavable.";

//Table 03
$lang['TS7500_03_01'] = "Tableros de mesa especiales [3-5]";
$lang['TS7500_03_02'] = "Carbon FloatLine";
$lang['TS7500_03_03'] = "Longitud";
$lang['TS7500_03_04'] = "Anchura en rieles estándar";
$lang['TS7500_03_05'] = "Rango de ajuste de la sección de piernas";
$lang['TS7500_03_06'] = "Rango de ajuste de la sección dorsal superior";
$lang['TS7500_03_07'] = "Rango de ajuste de la sección dorsal inferior";
$lang['TS7500_03_08'] = "Desplazamiento longitudinal";
$lang['TS7500_03_09'] = "Desplazamiento lateral";
$lang['TS7500_03_10'] = "[3] Dispositivos de mando disponibles: a distancia con cable y cargador para mando a distancia inalámbrico (móvil y en montaje de pared).<br>[4] Tableros H disponibles con colchonetas Balanced, CFL y ST26 disponibles con colchonetas Balanced o Comfort Plus.<br>[5] Los tableros para mesa universales también están disponibles en la variante lavable.";


$lang['PST500_01'] = "Dimensiones del tablero de la mesa";
$lang['PST500_02'] = "Peso neto de la mesa";
$lang['PST500_03'] = "Ajuste de altura";
$lang['PST500_04'] = "Trendelenburg/Trendelenburg inverso";
$lang['PST500_05'] = "Inclinación";
$lang['PST500_06'] = "Desplazamiento longitudinal";
$lang['PST500_07'] = '40 cm ± 0,5 cm (15,75""±0,2"") hacia la cabeza: 20 cm (7,9"")/hacia los pies: 20 cm (7,9"")';
$lang['PST500_08'] = "Rango de ajuste de la sección dorsal";
$lang['PST500_09'] = "Rango de ajuste de la sección de pierna";
$lang['PST500_10'] = "Posicionamiento cero";
$lang['PST500_11'] = "Nivelación simultánea automática de las secciones del tablero de la mesa";
$lang['PST500_12'] = "Capacidad de carga máxima";
$lang['PST500_footer'] = "*La capacidad de peso puede variar en función de la configuración de la mesa. Para obtener más información, consulta las instrucciones de uso.";


$lang['TS3000_01'] = "Ajuste de altura";
$lang['TS3000_02'] = "Trendelenburg/antitren- delenburg";
$lang['TS3000_03'] = "Inclinación";
$lang['TS3000_04'] = "Puesta a cero";
$lang['TS3000_05'] = "Nivelación motorizada de todas las secciones";
$lang['TS3000_06'] = "Desplazamiento longitudinal";
$lang['TS3000_07'] = "34 cm (± 1 cm)(parte superior: 19,5 cm, parte inferior: 14,5 cm)";
$lang['TS3000_08'] = "Rango de ajuste de la sección dorsal*";
$lang['TS3000_09'] = "Rango de ajuste de la sección de piernas";
$lang['TS3000_10'] = "Tablero de mesa (L x A)*";
$lang['TS3000_11'] = "Peso neto de la mesa";
$lang['TS3000_footer'] = "* En función de las preferencias del anestesista (acceso lateral o
craneal al paciente) la Mesa Quirúrgica TS3000 se monta con una
sección dorsal corta o larga. Esto puede ser beneficioso durante las
intervenciones ginecológicas y urológicas cuando al paciente está
colocado en decúbito supino sobre la mesa.";

$lang['TS7000DV_01'] = "Dimensiones de la mesa";
$lang['TS7000DV_02'] = "Capacidad de peso del paciente";
$lang['TS7000DV_03'] = "Ajuste de altura";
$lang['TS7000DV_04'] = "Posición de Trendelenburg y Trendelenburg inversa";
$lang['TS7000DV_05'] = "Inclinación";
$lang['TS7000DV_06'] = "Deslizamiento longitudinal";
$lang['TS7000DV_07'] = "Rango de ajuste de las articulaciones de la sección de las piernas";
$lang['TS7000DV_08'] = "Rango de ajuste de las articulaciones de la sección de la espalda";
$lang['TS7000DV_09'] = "Peso neto de la mesa";

?>