<?php
/*******************************************************************************
********************************************************************************
**************************** TRADUCCION AL PORTUGES ****************************
********************************************************************************
********************************************************************************/
$lang['leng_pt'] = 'Português';
$lang['leng_es'] = 'Espanhol';

$lang['leng_home'] = 'Começar';

$lang['leng_somos'] = 'AHORA SOMOS';

$lang['lbl_menu_contacto'] = 'Contato';
$lang['lbl_tlt_config'] = 'Configuração avançada';
	
$lang['lbl_tlt_compon'] = 'Componentes para Otimizar sua Sala de Cirurgia';
$lang['lbl_tlt_pads'] = 'Almofadas para Otimizar sua Sala de Cirurgia';

$lang['lbl_form_contacto'] = 'https://go.pardot.com/l/8232/2022-03-09/bz74qc';

$lang['lbl_tipos_procedimientos'] = 'Tipos de Procedimentos';
$lang['lbl_tipos_procedimientos_desc'] = 'TORI te recomendará os equipamentos e os componentes essenciais para otimizar cada procedimento cirúrgico';
$lang['lbl_tipos_quirofanos'] = 'Tipos de Sala de Cirurgia';
$lang['lbl_tipos_quirofanos_desc'] = 'SELECIONE O TIPO DE SALA QUE DESEJA AVALIAR E TORI TE OFERECERÁ A SOLUÇÃO CIRÚRGICA QUE MELHOR ATENDA À SUA NECESSIDADE.';
$lang['lbl_soluciones_title'] = 'Soluções Recomendadas';

$lang['lbl_ver_mas'] = 'Saiba mais';

$lang['lbl_cantidad'] = 'QUANTIDADE';
$lang['lbl_procedimientos'] = 'Procedimentos';

$lang['lbl_componentes'] = 'COMPONENTES';
$lang['lbl_componentes_min'] = 'Componentes';
$lang['lbl_pads'] = 'ALMOFADAS';
$lang['lbl_pads_min'] = 'Almofadas';
$lang['lbl_solicitar_mas_info'] = 'SOLICITAR INFORMAÇÕES';
$lang['lbl_posicion'] = 'VER POSICIONAMENTO';

$lang['lbl_mas_info'] = 'Más información';
$lang['lbl_obtener'] = 'Obtener ahora';

$lang['lbl_descargar'] = 'Download';
$lang['lbl_video_micro'] = 'Vídeo sobre Microclima';

$lang['lbl_superficie'] = 'Superficies';

$lang['lbl_probabilidades'] = 'Veja as probabilidades de LPP';
$lang['lbl_versuperficies'] = 'Veja superfícies';
$lang['lbl_versuperficies_todas'] = 'Veja todas as superfícies';

$lang['lbl_estaticas'] = 'Estáticas';
$lang['lbl_mixta'] = 'Mixtas';
$lang['lbl_dinamica'] = 'Dinámicas';

$lang['lbl_niveles'] = 'Classificação de Risco';

$lang['lbl_nivel_01'] = 'BAIXO';
$lang['lbl_nivel_02'] = 'BAIXO A MODERADO';
$lang['lbl_nivel_03'] = 'MODERADO';
$lang['lbl_nivel_04'] = 'ALTO';
$lang['lbl_nivel_05'] = 'MULTO ALTO';

$lang['lbl_piel_01'] = 'Piel con lesión';
$lang['lbl_piel_02'] = 'Recomendação';


$lang['lbl_config_avanzada_01'] = 'SELECIONE UM PROCEDIMENTO';
$lang['lbl_config_avanzada_02'] = 'SELECIONE UMA SALA DE CIRURGIA';
$lang['lbl_config_avanzada_03'] = 'SELECIONE UM PRODUTO';
$lang['lbl_config_avanzada_04'] = 'VER CONFIGURAÇÃO';


$lang['lbl_riesgo_01'] = 'Baixo Risco';
$lang['lbl_riesgo_01_desc'] = 'Recomendamos o uso de superfícies estáticas.';

$lang['lbl_riesgo_02'] = 'Risco Moderado';
$lang['lbl_riesgo_02_desc'] = 'Recomendamos o uso de superfícies mistas ou dinâmicas.';

$lang['lbl_riesgo_03'] = 'Alto Risco';
$lang['lbl_riesgo_03_desc'] = 'Recomendamos o uso de superfícies dinâmicas com tecnologia de Baixa Pressão Contínua (CLP), Baixa Pressão Alternada (ALP) ou Low Air Loss (LAL).';
$lang['lbl_riesgos_txt'] = 'Superfícies especializadas recomendadas para:';

$lang['menu_nivel'] = 'Níveis';
$lang['menu_clasificacion'] = 'Guia de Superfícies/Assistência';

$lang['txt_sup_camas_descr'] = 'Escolha o tipo ideal de superfície hospitalar de acordo com a Classificação de Risco';

$lang['txt_niveles_titulo'] = 'Classificação de Risco por níveis';

/** LEVELES **/
$lang['lbl_bajo'] = 'BAIXO';
$lang['lbl_medio'] = 'MODERADO';
$lang['lbl_alto'] = 'ALTO';

$lang['lbl_nombre_link_bajo'] = 'Veja superfícies para baixo risco';
$lang['lbl_nombre_link_medio'] = 'Veja superfícies para risco moderado';
$lang['lbl_nombre_link_alto'] = 'Veja superfícies para alto risco';

$lang['txt_t_01'] = 'Baixo Risco';
$lang['txt_d_01'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Para pacientes com risco baixo de desenvolver lesões por pressão, é recomendado o uso de superfícies estáticas de alta qualidade e produzidas de diferentes materiais como gel, espuma e materiais viscoelásticos.</p>
	<a href="'.base_url().'asistencia/superficies?nivel=bajo" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Veja superfícies para baixo risco</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';
$lang['txt_t_02'] = 'Risco Moderado';
$lang['txt_d_02'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Pacientes com risco moderado de desenvolver lesões por pressão necessitam de um nível mais alto de cuidado, exigindo superfícies mistas com sistema de redistribuição de pressão ou superfícies dinâmicas com tecnologia de Baixa Pressão Contínua (CLP), Baixa Pressão Alteranada (ALP) ou Low Air Loss (LAL).</p>
	<a href="'.base_url().'asistencia/superficies?nivel=medio" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Veja superfícies para risco moderado</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';

$lang['txt_t_03'] = 'Alto Risco';
$lang['txt_d_03'] = '<p style="color: #fff;margin: 20px 0 50px;max-width: 65%;">Pacientes com risco alto de desenvolver lesões por pressão necessitam de um maior cuidado em combinação com o uso de superfícies dinâmicas com tecnologia de Baixa Pressão Contínua, Baixa Pressão Alternada (ALP) ou Low Air Loss (LAL).</p>
	<a href="'.base_url().'asistencia/superficies?nivel=alto" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Veja superfícies para alto risco</a>
	<a href="'.base_url().'escalas/braden/" style="background: #5369E2;;padding: 10px 30px;font-size: 15px;">Escala Braden</a>';

/** ESCALA BRADEN **/

$lang['braden_titulo'] = 'Para determinar o Nível de Risco de lesões por pressão em desenvolvimento, selecione as opções que melhor descrevem o seu paciente';

$lang['braden_titulo_02'] = 'ESCALA BRADEN';

$lang['braden_columna_titulo_01'] = 'Percepção Sensorial';
$lang['braden_columna_01_item_01'] = '1. Totalmente limitada';
$lang['braden_columna_01_item_02'] = '2. Muito limitada';
$lang['braden_columna_01_item_03'] = '3. Levemente limitada';
$lang['braden_columna_01_item_04'] = '4. Sem limitações';

$lang['braden_columna_titulo_02'] = 'Umidade';
$lang['braden_columna_02_item_01'] = '1. Constantemente úmida';
$lang['braden_columna_02_item_02'] = '2. Muito úmida';
$lang['braden_columna_02_item_03'] = '3. Ocasionalmente úmida';
$lang['braden_columna_02_item_04'] = '4. Raramente úmida';

$lang['braden_columna_titulo_03'] = 'Atividade';
$lang['braden_columna_03_item_01'] = '1. Acamado';
$lang['braden_columna_03_item_02'] = '2. Confinado à cadeira';
$lang['braden_columna_03_item_03'] = '3. Anda ocasionalmente';
$lang['braden_columna_03_item_04'] = '4. Anda frequentemente';

$lang['braden_columna_titulo_04'] = 'Mobilidade';
$lang['braden_columna_04_item_01'] = '1. Totalmente imóvel';
$lang['braden_columna_04_item_02'] = '2. Muito limitada';
$lang['braden_columna_04_item_03'] = '3. Levemente limitada';
$lang['braden_columna_04_item_04'] = '4. Excelente';

$lang['braden_columna_titulo_05'] = 'Nutrição';
$lang['braden_columna_05_item_01'] = '1. Muito pobre';
$lang['braden_columna_05_item_02'] = '2. Provavelmente inadequada';
$lang['braden_columna_05_item_03'] = '3. Adequada';
$lang['braden_columna_05_item_04'] = '4. Excelente';

$lang['braden_columna_titulo_06'] = 'Fricção e Cisalhamento';
$lang['braden_columna_06_item_01'] = '1. Problema';
$lang['braden_columna_06_item_02'] = '2. Problema Potencial';
$lang['braden_columna_06_item_03'] = '3. Sem problema aparente';

$lang['braden_nivel'] = 'Risco Nível:';
$lang['braden_desc'] = 'Pontuação: ≤16 Risco Baixo, ≤14 Risco Médio y ≤12 Risco Alto';

$lang['probabilidades_titulo'] = 'Probabilidade de desenvolver LPP conforme a superfície de apoio utilizada:';
$lang['probabilidades_item_01'] = '21.8% <small>SUPERFÍCIE DE ESPUMA PADRÃO</small>';
$lang['probabilidades_item_02'] = '8.9% <small>SUPERFÍCIE DE BAIXA PRESSÃO CONSTANTE</small>';
$lang['probabilidades_item_03'] = '6.8% <small>SUPERFÍCIE DE PRESSÃO ALTERNADA</small>';

$lang['algoritmo_home_titulo'] = 'Algoritmo de Assistência de Superfícies';
$lang['algoritmo_home_desc'] = 'baseado em Consenso e Evidências';
$lang['algoritmo_home_btn'] = 'Iniciar';
$lang['algoritmo_home_modal'] = 'Superficies especiales para el manejo de la presión (SEMP)<br><br>
		Según el documento XIII del GNEAUPP, las superficies de apoyo (SA) o superficies especiales para el manejo de la presión (SEMP) se definen como la superficie o dispositivo especializado, cuya configuración física y/o estructural permite la redistribución de la presión, así como otras funciones terapéuticas añadidas para el manejo de las cargas tisulares, fricción, cizalla y/o microclima, y que abarca el cuerpo de un individuo o una parte del mismo, según las diferentes posturas funcionales posibles.';

$lang['algoritmo_asistencia_titulo'] = '
		<h3 style="color:#fff">Presença de risco de lesões por pressão</h3>';

$lang['algoritmo_asistencia_bajada'] = 'Selecione a variável correspondente';
$lang['algoritmo_pin'] = 'Recomendação';
$lang['algoritmo_pin_01'] = 'Sim, está em risco';
$lang['algoritmo_pin_02'] = 'Não está em risco';

$lang['algoritmo_pcl'] = 'Pele com lesão';

$lang['en_riesgo'] = 'Em risco';

$lang['algoritmo_piel_titulo'] = '<h3 style="color:#fff;font-size: 40px;">Avaliação do risco de lesões por pressão</h3>';

$lang['algoritmo_piel_bajada'] = 'Selecione o nível de risco conforme a Escala de Braden';

$lang['algoritmo_piel_btn_braden'] = 'Consulte a Escala de Braden';

$lang['algoritmo_piel_riesgo_01'] = 'Baixo Risco';

$lang['algoritmo_piel_riesgo_desc_01'] = '<strong>Mais de 15 pontos</strong> na Escala de Braden';

$lang['algoritmo_piel_riesgo_02'] = 'Risco Moderado';
$lang['algoritmo_piel_riesgo_desc_02'] = '<strong>De 13 a 14 pontos</strong> na Escala de Braden';

$lang['algoritmo_piel_riesgo_03'] = 'ALTO RISCO';
$lang['algoritmo_piel_riesgo_desc_03'] = '<strong>Menos de 12 puntos</strong> na Escala de Braden';

$lang['disclamer'] = 'Para mais informações, consulte seu especialista.<br>As restrições de configuração são aplicadas de acordo com os registros de cada país';


$lang['hlbax'] = 'Hillrom é parte da Baxter';

$lang['frase_01'] = 'Você sabia que, em média, os hospitais têm 20 salas de cirurgia geral nos EUA, 6-7 na América Latina e 12-14 na Europa?';
$lang['frase_02'] = 'Dois terços dos hospitais têm salas de cirurgia híbridas, entre 2 e 6 por hospital. Salas cirúrgicas robóticas são menos frequentes na América Latina';
$lang['frase_03'] = 'As salas de cirurgia geral normalmente são atualizadas a cada 5 anos nos EUA, América Latina e Reino Unido';
$lang['frase_04'] = '. Qualidade, facilidade de uso, impacto na eficiência e segurança do fluxo de trabalho e custo-benefício são importantes na escolha dos produtos.';
$lang['frase_05'] = 'Ao adquirir soluções de centro cirúrgico, a qualidade vem em primeiro lugar, seguida pela segurança e, em seguida, pelo custo-benefício.';
$lang['frase_06'] = 'A maioria dos hospitais tem salas de cirurgia geral onde os cirurgiões e diretores dos centros cirurgicos passam a maior parte do tempo.';
$lang['frase_07'] = 'Mais da metade dos hospitais têm salas cirurgicas híbridas; salas de cirurgia robóticas são menos frequentes.';
$lang['frase_08'] = 'A informação sobre o produto é de grande interesse para os hospitais que desejam adquirir novos equipamentos cirúrgicos.';

$lang['TL3000_01'] = "Variantes da cúpula";
$lang['TL3000_02'] = "Nível de iluminação Ec a 1 m (lx)";
$lang['TL3000_03'] = "Alteração de tamanho padrão – variação de distância (cm)";
$lang['TL3000_04'] = "Tamanho padrão (d10) a 1 m (cm)";
$lang['TL3000_05'] = "Temperatura da cor (K)";
$lang['TL3000_06'] = "Índice de reprodução de cor (Ra)";
$lang['TL3000_07'] = "Vida útil média dos LEDs (h)";
$lang['TL3000_08'] = "Controle de luz adaptativo (ALC)";
$lang['TL3000_09'] = "Preparação da câmera";
$lang['TL3000_10'] = "Os valores estão sujeitos a uma tolerância geral da indústria de ± 10%";


// Table 01
$lang['TRUPORT_01_01'] = "PADRÃO";
$lang['TRUPORT_01_02'] = "OPÇÕES ADICIONAIS COM TRILHO MPC";
$lang['TRUPORT_01_03'] = "Braços de suporte";
$lang['TRUPORT_01_04'] = "Alcance do braço de suporte";
$lang['TRUPORT_01_05'] = "Sistema de braço simples: até 1300 mm<br>Sistema de braço duplo: até 3200 mm";
$lang['TRUPORT_01_06'] = "Sistema de freio";
$lang['TRUPORT_01_07'] = "fricção pneumático";
$lang['TRUPORT_01_08'] = "sistema de freio eletropneumático<br>sistema de freio eletromagnético [1]";
$lang['TRUPORT_01_09'] = "Suporte de teto";
$lang['TRUPORT_01_10'] = "Espaçamento de suspensão do teto de até<br>2500 mm / à prova de terremotos";
$lang['TRUPORT_01_11'] = "Fixo ou com ajuste de altura";
$lang['TRUPORT_01_12'] = "Cabeça de suporte";
$lang['TRUPORT_01_13'] = "Dimensões";
$lang['TRUPORT_01_14'] = "Integração de sistema de elevação motorizada";
$lang['TRUPORT_01_15'] = "Comprimentos";
$lang['TRUPORT_01_16'] = "até 1750 mm";
$lang['TRUPORT_01_17'] = "[1] Específico por país.";

// Table 02
$lang['TRUPORT_02_01'] = "Módulos de abastecimento";
$lang['TRUPORT_02_02'] = "Reorganização de dados";
$lang['TRUPORT_02_03'] = "Reorganização de gás";
$lang['TRUPORT_02_04'] = "Reorganização do módulo de energia elétrica";
$lang['TRUPORT_02_05'] = "Componentes da estação de trabalho";
$lang['TRUPORT_02_06'] = "Montagem sem ferramentas com mecanismo de clique";
$lang['TRUPORT_02_07'] = "Componentes Eletrônicos";
$lang['TRUPORT_02_08'] = "Módulos AmbientLine";
$lang['TRUPORT_02_09'] = "Chão AmbientLine, Teto AmbientLine";
$lang['TRUPORT_02_10'] = "Chão AmbientLine, Teto AmbientLine, Mesa AmbientLine";

// Table 03
$lang['TRUPORT_03_01'] = "Prateleiras e gavetas";
$lang['TRUPORT_03_02'] = "Capacidade de carga";
$lang['TRUPORT_03_03'] = "Prateleira de carga média: 50 kg<br>Prateleira de carga pesada: 80 kg";
$lang['TRUPORT_03_04'] = "Elemento de operação nas prateleiras";
$lang['TRUPORT_03_05'] = "Para a prateleira de carga média";
$lang['TRUPORT_03_06'] = "Para a prateleira de carga pesada";
$lang['TRUPORT_03_07'] = "Dimensões das prateleiras<br>(largura x profundidade) ";
$lang['TRUPORT_03_08'] = "Prateleira de carga média:<br>
530, 630 x 480 mm<br>
Prateleira de carga pesada:<br>
430[2], 530, 630, 730 x 480 mm<br>
Kit de extensão de profundidade de 70 mm";
$lang['TRUPORT_03_09'] = "Prateleira de carga média:<br>
530, 630 x 480 mm<br>
Prateleira de carga pesada:<br>
430[2], 530, 630, 730 x 480 mm<br>
Kit de extensão de profundidade de 70 mm";
$lang['TRUPORT_03_10'] = "Numero maximo de gavetas em uma prateleira";
$lang['TRUPORT_03_11'] = "Prateleira de carga média: max. 2<br>Prateleira de carga pesada: max. 4";
$lang['TRUPORT_03_12'] = "[2] Também disponivel nos tamanhos 430mm largura por 260mm de profundidade.";

$lang['PST300_01'] = "Dimensões de topo de mesa";
$lang['PST300_02'] = "Peso líquido da mesa";
$lang['PST300_03'] = "Ajuste da altura";
$lang['PST300_04'] = "Trendelenburg/Anti-Trendelenburg";
$lang['PST300_05'] = "Inclinação";
$lang['PST300_06'] = "Deslocamento longitudina";
$lang['PST300_07'] = '34 cm (± 1 cm)/13,4"(±0,4")(cabeça) 19,5 cm (7,7")/ (pés): 14,5 cm (5,7”)';
$lang['PST300_08'] = "Segmento dorsal articulado";
$lang['PST300_09'] = "Segmento das pernas articulado";
$lang['PST300_10'] = "Posição zero";
$lang['PST300_11'] = "Nivelamento Motorizado de todos os segmentos";
$lang['PST300_12'] = "Max. peso do paciente";
$lang['PST300_13'] = "Acessórios compatíveis com extremidade inferior";
$lang['PST300_14'] = "Acessórios compatíveis com topo";

//Table 01
$lang['TS7500_01_01'] = "Colunas da tabela";
$lang['TS7500_01_02'] = "Coluna da tabela SC";
$lang['TS7500_01_03'] = "Coluna da tabela FC";
$lang['TS7500_01_04'] = "Coluna da tabela SMC, Hybrid MC<small>2</small>";
$lang['TS7500_01_05'] = "Coluna da tabela Hybrid SC, FC";

$lang['TS7500_01_06'] = "Rotação";
$lang['TS7500_01_tope'] = "sem parar";
$lang['TS7500_01_ctope'] = "com parada";
$lang['TS7500_01_07'] = "Altura da coluna";
$lang['TS7500_01_08'] = "Altura mín. incl. tampo de mesa universal H";
$lang['TS7500_01_09'] = "Alavanca";
$lang['TS7500_01_10'] = "Seção de coluna (L x A)";
$lang['TS7500_01_11'] = "Trend./ inclinação";
$lang['TS7500_01_12'] = "Altura da placa base";
$lang['TS7500_01_13'] = "Carregamento máx. <small>1</small>";
$lang['TS7500_01_14'] = "[1] Com limitações dependendo da posição do paciente.<br>[2] A alavanca de limpeza para levantar a coluna móvel permite a limpeza.";

//Table 02
$lang['TS7500_02_01'] = "Tampos de mesa universais [3-5]";
$lang['TS7500_02_02'] = "Comprimento";
$lang['TS7500_02_03'] = "Largura em trilhos padrão";
$lang['TS7500_02_04'] = "Faixa de ajuste da seção da perna";
$lang['TS7500_02_05'] = "Faixa de ajuste da parte superior das costas";
$lang['TS7500_02_06'] = "Faixa de ajuste da parte inferior das costas";
$lang['TS7500_02_07'] = "Trendelenburg";
$lang['TS7500_02_08'] = "Deslocamento longitudinal";
$lang['TS7500_02_09'] = "Deslocamento lateral";
$lang['TS7500_02_10'] = "[3] Dispositivos de controle disponíveis: controle remoto com fio e carregador para controle remoto sem fio (montagem móvel e na parede).<br>[4] Placas H disponíveis com tapetes Balanced, CFL e ST26 disponíveis com tapetes Balanced ou Comfort Plus.<br>[5] Os tampos de mesa universais também estão disponíveis na variante lavável.";

//Table 03
$lang['TS7500_03_01'] = "Tampos de mesa especiais [3-5]";
$lang['TS7500_03_02'] = "Carvão FloatLine";
$lang['TS7500_03_03'] = "Comprimento";
$lang['TS7500_03_04'] = "Largura em trilhos padrão";
$lang['TS7500_03_05'] = "Faixa de ajuste da seção da perna";
$lang['TS7500_03_06'] = "Faixa de ajuste da parte superior das costas";
$lang['TS7500_03_07'] = "Faixa de ajuste da parte inferior das costas";
$lang['TS7500_03_08'] = "Deslocamento longitudinal";
$lang['TS7500_03_09'] = "Deslocamento lateral";
$lang['TS7500_03_10'] = "[3] Dispositivos de controle disponíveis: controle remoto com fio e carregador para controle remoto sem fio (montagem móvel e na parede).<br>[4] Placas H disponíveis com tapetes Balanced, CFL e ST26 disponíveis com tapetes Balanced ou Comfort Plus.<br>[5] Os tampos de mesa universais também estão disponíveis na variante lavável.";

$lang['PST500_01'] = "Dimensões de topo de mesa";
$lang['PST500_02'] = "Peso líquido da mesa";
$lang['PST500_03'] = "Ajuste da altura";
$lang['PST500_04'] = "Trendelenburg/Anti-Trendelenburg";
$lang['PST500_05'] = "Inclinação";
$lang['PST500_06'] = "Deslocamento longitudina";
$lang['PST500_07'] = '40 cm ± 0,5 cm (15,75""""±0,2"""") (cabeça) 20 cm (7,9"""")/ (pés): 20 cm (7,9”)';
$lang['PST500_08'] = "Segmento dorsal articulado";
$lang['PST500_09'] = "Segmento das pernas articulado";
$lang['PST500_10'] = "Posição zero";
$lang['PST500_11'] = "Nivelamento Motorizado de todos os segmentos";
$lang['PST500_12'] = "Max. peso do paciente";
$lang['PST500_footer'] = "*A capacidade de peso pode variar dependendo da configuração da mesa. Para obter mais informações, consulte as instruções de uso.";

$lang['TS3000_01'] = "Altura da mesa cirúrgica";
$lang['TS3000_02'] = "Trendelenburg / Anti- Trendelenburg";
$lang['TS3000_03'] = "Inclinação";
$lang['TS3000_04'] = "Posição zero";
$lang['TS3000_05'] = "Nivelamento Motorizado de todos os segmentos";
$lang['TS3000_06'] = "Deslocamento longitudinal";
$lang['TS3000_07'] = "34 cm (1 cm) /13,4″ (0,4″)(cabeça) 19,5 cm (1 cm) /7,7″(pés) 14,5 cm (1 cm)/5,7″";
$lang['TS3000_08'] = "Segmento dorsal articulado*";
$lang['TS3000_09'] = "Segmento das pernas articulado";
$lang['TS3000_10'] = "Topo da Mesa geral (L x W)*";
$lang['TS3000_11'] = "Peso da mesa";
$lang['TS3000_footer'] = "* Dependendo das preferências do anestesista - acesso lateral ou craniano ao paciente - a Mesa TS 3000 vem a seção de costas longas ou curta. Isso pode trazer benefícios durante procedimentos ginecológicos e urológicos, quando o paciente é deslocado para a frente sobre a mesa.";

$lang['TS7000DV_01'] = "Dimensões do tampo da mesa";
$lang['TS7000DV_02'] = "Capacidade de peso do paciente";
$lang['TS7000DV_03'] = "Ajuste de altura";
$lang['TS7000DV_04'] = "Trendelenburg/posição de Trendelenburg invertida";
$lang['TS7000DV_05'] = "Inclinação";
$lang['TS7000DV_06'] = "Deslizamento longitudinal";
$lang['TS7000DV_07'] = "Intervalo de ajuste das articulações da parte da perna";
$lang['TS7000DV_08'] = "Intervalo de ajuste das articulações da parte das costas";
$lang['TS7000DV_09'] = "Peso líquido da mesa";

?>