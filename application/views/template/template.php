<!DOCTYPE html>
<html class="html-hidde-overflow">
   <head>
   	  <meta charset="utf-8" />
      <title>Tori - <?= $title ?></title>
	  <meta name="description" content="">
	  <meta name="keywords" content="">
	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
	  
	  <meta property="og:title" content="Tori" />
	  <meta property="og:type" content="<?= $ogType ?>" />
	  <meta property="og:url" content="<?php echo base_url(uri_string()) ?>" />
	  <meta property="og:image" content="" />
	  <meta property="og:description" content="La Base de la Atención Quirúrgica" />
	  
	  
	  <link rel="shortcut icon" type="image/png" href="<?=base_url()?>asset/img/favicon.png"> 
	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.2/css/bulma.min.css'>

	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	  
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.0/css/all.css" integrity="sha384-OLYO0LymqQ+uHXELyx93kblK5YIS3B2ZfLGBmsJaUyor7CpMTBsahDHByqSuWW+q" crossorigin="anonymous">

	  <link rel='stylesheet' href='https://unpkg.com/xzoom/dist/xzoom.css'>

	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'>
	  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/main.css?v=<?=time()?>" type="text/css" media="all" />

	  <script>var base_url = "<?php echo base_url().$this->config->item('language_abbr') ?>";</script>
	  <?= $_styles ?>
	  <!-- Global site tag (gtag.js) - Google Analytics -->
	  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-232662426-1"></script>
	  <script>
	    window.dataLayer = window.dataLayer || [];
	    function gtag(){dataLayer.push(arguments);}
	    gtag('js', new Date());

	    gtag('config', 'UA-232662426-1');
	  </script>

   </head>
   <body class="html-hidde-overflow">
	  <?= $header ?>        
	  <?= $content ?>
   	<?= $footer ?>
	  	
	  <div class="btn-home">
	  	<a href="<?=base_url().$this->config->item('language_abbr')?>/">
	  		<i class="fas fa-home"></i>
	  	</a>
	  </div>
	  	
	  <div class="btn-contact">
	  	<a href="<?=base_url().$this->config->item('language_abbr')?>/contacto/">
	  		<i class="fas fa-envelope"></i>
	  	</a>
	  </div>	

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
	  <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	 <!-- Latest compiled and minified JavaScript -->
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

	 <!-- (Optional) Latest compiled and minified JavaScript translation files -->
	 
	  <script src='https://unpkg.com/xzoom/dist/xzoom.min.js'></script>

      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      

      <script defer type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js?v=<?=time()?>"></script> 


	  <?= $_scripts ?>
	  <script type="text/javascript">
	  	$(window).on('load', function() {
	  	    $('.btn-home').addClass('transition-load');
	  	    $('.btn-contact').addClass('transition-load');
	  	})
	  </script>
	  <script type="text/javascript">
	  	$('#slideproduct').owlCarousel({
	  	  loop: false,
	  	  margin: 0,
	  	  nav: true,
	  	  autoplay: false,
	  	  autoplayHoverPause: false,
	  	  autoHeight: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1,
              autoHeight:true
	  	    },
	  	    600: {

	  	      items: 1,
              autoHeight:true
	  	    },
	  	    1000: {
	  	      items: 1,
              autoHeight:true
	  	    }
	  	  }
	  	});

	  	$('#slideposiciones').owlCarousel({
	  	  loop: true,
	  	  margin: 0,
	  	  nav: true,
	  	  autoplay: false,
	  	  autoplayHoverPause: false,
	  	  autoHeight: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1,
              autoHeight:true
	  	    },
	  	    600: {

	  	      items: 1,
              autoHeight:true
	  	    },
	  	    1000: {
	  	      items: 2,
              autoHeight:true
	  	    }
	  	  }
	  	});

	  	$('#slideesp').owlCarousel({
	  	  loop: true,
	  	  margin: 0,
	  	  nav: true,
	  	  autoplay: false,
	  	  autoplayHoverPause: false,
	  	  autoHeight: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1,
              autoHeight:true
	  	    },
	  	    600: {

	  	      items: 1,
              autoHeight:true
	  	    },
	  	    1000: {
	  	      items: 2,
              autoHeight:true
	  	    }
	  	  }
	  	});
	  	$('#slideesp02').owlCarousel({
	  	  loop: true,
	  	  margin: 0,
	  	  nav: true,
	  	  autoplay: false,
	  	  autoplayHoverPause: false,
	  	  autoHeight: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1,
              autoHeight:true
	  	    },
	  	    600: {

	  	      items: 1,
              autoHeight:true
	  	    },
	  	    1000: {
	  	      items: 2,
              autoHeight:true
	  	    }
	  	  }
	  	});

	  	$('#grillahome').owlCarousel({
	  	  loop: true,
	  	  margin: 20,
	  	  nav: true,
	  	  autoplay: false,
	  	  autoplayHoverPause: false,
	  	  autoHeight: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1,
              autoHeight:true
	  	    },
	  	    600: {

	  	      items: 1,
              autoHeight:true
	  	    },
	  	    1000: {
	  	      items: 4,
              autoHeight:true
	  	    }
	  	  }
	  	});

	  	$('#grillahomeresponsive').owlCarousel({
	  	  loop: true,
	  	  margin: 20,
	  	  nav: true,
	  	  autoplay: false,
	  	  autoplayHoverPause: false,
	  	  autoHeight: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1,
              autoHeight:true
	  	    },
	  	    600: {

	  	      items: 1,
              autoHeight:true
	  	    },
	  	    1000: {
	  	      items: 1,
              autoHeight:true
	  	    }
	  	  }
	  	});

	  	$('#componentes').owlCarousel({
	  	  loop: true,
	  	  margin: 20,
	  	  nav: true,
	  	  autoplay: false,
	  	  autoplayHoverPause: false,
	  	  autoHeight: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1,
              autoHeight:true
	  	    },
	  	    600: {

	  	      items: 1,
              autoHeight:true
	  	    },
	  	    1000: {
	  	      items: 4,
              autoHeight:true
	  	    }
	  	  }
	  	});
      </script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/typed.js/1.1.4/typed.min.js'></script>
      <script type="text/javascript">
          $(".form-pdf").submit(function(event){
            event.preventDefault(); //prevent default action 
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = $(this).serialize(); //Encode form elements for submission
            
            $.ajax({
                url : post_url,
                type: request_method,
                data : form_data,
                beforeSend: function() {
                    // setting a timeout
                    $('.enviar').addClass('disable');
                    //$('.message').hide("slow");
                },
                success: function(response){
                     $('.enviar').removeClass('disable');
                     window.open(response, '_blank');
                     $('.form-pdf')[0].reset();
                }
            });
          });

          $(".max-w-img-tori").click(function() {
            $('.tori-robot').addClass('active-robot');
            $('.box-content-robot').show();

            setTimeout(function() {

            		var r_text = new Array ();
            		r_text[0] = "<?=$this->lang->line('frase_01')?>";
            		r_text[1] = "<?=$this->lang->line('frase_02')?>";
            		r_text[2] = "<?=$this->lang->line('frase_03')?>";
            		r_text[3] = "<?=$this->lang->line('frase_04')?>";
            		r_text[4] = "<?=$this->lang->line('frase_05')?>";
            		r_text[5] = "<?=$this->lang->line('frase_06')?>";
            		r_text[6] = "<?=$this->lang->line('frase_07')?>";
            		r_text[7] = "<?=$this->lang->line('frase_08')?>";

            		var i = Math.floor(r_text.length * Math.random());

                $('#intro').typed({
                  strings: [
                    r_text[i]
                  ],
                  typeSpeed: 1,
                  contentType: 'html'
                });
              }, 500);

          });

          $(".close-roboto-box").click(function() {
            $('.tori-robot').removeClass('active-robot');
            $('.box-content-robot').hide();
          });



      </script>
   </body>
</html>