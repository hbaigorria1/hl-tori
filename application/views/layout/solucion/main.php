<section class="home-section w-100" style="background: #f3f4fb;height:auto;">
	<div class="container d-flex flex-column">
		<div class="d-block" style="margin-bottom:30px">
			<h4><?=$this->lang->line('lbl_soluciones_title')?></h4>
			<p><a href="<?=base_url().$this->config->item('language_abbr')?>/"><?=$this->lang->line('leng_home')?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=4"><?=$this->lang->line('lbl_procedimientos')?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=4"><?=$procedimiento[0]->nombre?></a> > Sala <?=$quirofano[0]->nombre?></p>
		</div>
		<div id="grillahome" class="owl-carousel owl-theme slider-products d-none d-sm-block" style="margin-bottom:auto;">
			<?php foreach($productos as $prd): ?>
				<div class="item text-center">
					<div class="content-product">
						<div class="icon-svg" style="background-color: <?=$quirofano[0]->color?>;mask: url('<?=base_url()?>asset/img/uploads/<?=$prd->icono?>') no-repeat center / contain;-webkit-mask: url('<?=base_url()?>asset/img/uploads/<?=$prd->icono?>') no-repeat center / contain;"></div>
						<h3 stle="color:<?=$quirofano[0]->color?>"><?=$prd->titulo?></h3>
						<p><?=$prd->description?></p>
						<div class="product-back" style="background:url('<?=base_url()?>asset/img/uploads/<?=$prd->background?>');"></div>
					</div>
					<a href="<?=base_url().$this->config->item('language_abbr')?>/producto?ver=<?=$prd->uniq?>&procedmiento=<?=$_GET['procedmiento']?>&quiofano=<?=$_GET['quiofano']?>" class="btn-link-product" style="background:<?=$quirofano[0]->color?>;"><?=$this->lang->line('lbl_ver_mas')?></a>
				</div>
			<?php endforeach; ?>
		</div>

		<div id="grillahomeresponsive" class="owl-carousel owl-theme slider-products d-block d-sm-none" style="margin-bottom:auto;margin-top: 30px;">
			<?php foreach($productos as $prd): ?>
				<div class="item text-center">
					<div class="content-product">
						<div class="icon-svg" style="background-color: <?=$quirofano[0]->color?>;mask: url('<?=base_url()?>asset/img/uploads/<?=$prd->icono?>') no-repeat center / contain;-webkit-mask: url('<?=base_url()?>asset/img/uploads/<?=$prd->icono?>') no-repeat center / contain;"></div>
						<h3 stle="color:<?=$quirofano[0]->color?>"><?=$prd->titulo?></h3>
						<p><?=$prd->description?></p>
						<div class="product-back" style="background:url('<?=base_url()?>asset/img/uploads/<?=$prd->background?>');"></div>
					</div>
					<a href="<?=base_url().$this->config->item('language_abbr')?>/producto?ver=<?=$prd->uniq?>&procedmiento=<?=$_GET['procedmiento']?>&quiofano=<?=$_GET['quiofano']?>" class="btn-link-product" style="background:<?=$quirofano[0]->color?>;"><?=$this->lang->line('lbl_ver_mas')?></a>
				</div>
			<?php endforeach; ?>
		</div>


	</div>
</section>