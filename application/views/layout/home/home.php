<section class="home-section" style="background: url('<?php echo base_url() ?>asset/img/uploads/<?=$config[0]->background?>')">
	<div>
		<h4><?=$config[0]->title?></h4>
		<p><?=$config[0]->desc?></p>
	</div>
    <div class="btns-home w-100 d-flex align-items-center justify-content-center flex-wrap">
    	<?php foreach($tipos as $tp): ?>
    		<?php if($tp->uniq == 4): ?>
				<a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=<?=$tp->uniq?>" class="botton-home" style="background:<?=$tp->color?>">
					<h3><?=$tp->nombre?></h3>
				</a>
    		<?php else: ?>
    			<a href="<?=base_url().$this->config->item('language_abbr')?>/configuracion-avanzada/" class="botton-home" style="background:<?=$tp->color?>">
    				<h3><?=$tp->nombre?></h3>
    			</a>
    		<?php endif; ?>
		<?php endforeach; ?>
    </div>
</section>

<div class="tori-robot">
	<div class="box-content-robot">
		<div class="close-roboto-box">X</div>
		<p id="intro"></p>
	</div>
	<img src="<?=base_url()?>asset/img/tori-icon.png" class="img-fluid max-w-img-tori">
	<div class="number">1</div>
</div>