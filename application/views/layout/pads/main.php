<section class="componente-section w-100" style="background: #f3f4fb">
	<div class="container d-flex flex-column h-100">
		<div class="d-block w-100" style="margin-bottom:auto">
			<h4><?=$this->lang->line('lbl_tlt_pads')?></h4>
			<p><a href="<?=base_url().$this->config->item('language_abbr')?>/"><?=$this->lang->line('leng_home')?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=4"><?=$this->lang->line('lbl_procedimientos')?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=4"><?=$procedimiento[0]->nombre?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/quirofanos?procedmiento=<?=$procedimiento[0]->uniq?>">Sala <?=$quirofano[0]->nombre?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/producto?ver=<?=$producto[0]->uniq?>&procedmiento=<?=$procedimiento[0]->uniq?>&quiofano=<?=$quirofano[0]->uniq?>"><?=$producto[0]->titulo?></a> > <?=$this->lang->line('lbl_pads_min')?></p>
		</div>
		<div class="content-componentes" style="margin-bottom:auto;height:70%;">
			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				<?php foreach($posicionamientos_pads as $key => $posicion): ?>
					  <li class="nav-item">
					    <a class="nav-link <?php if($key == 0): echo "active"; endif; ?>" id="pos<?=$key?>-tab" data-toggle="pill" href="#pos<?=$key?>" role="tab" aria-controls="pos<?=$key?>" aria-selected="true"><?=$posicion->posicion?></a>
					  </li>
				
			   <?php endforeach; ?>
			</ul>
			<div class="tab-content h-100" id="pills-tabContent">
			   <?php foreach($posicionamientos_pads as $key => $posicion): ?>
			  	<div class="tab-pane fade h-100 <?php if($key == 0): echo "show active"; endif; ?>" id="pos<?=$key?>" role="tabpanel" aria-labelledby="pos<?=$key?>-tab">
			  		<div id="componentes<?=$key?>" class="owl-carousel owl-theme slider-products h-100" >
			  			<?php $pads = $this->page_model->get_pads($posicion->uniq, $language);
			  			$padsDiv = array_chunk($pads, 2);?>
				  			<?php foreach($padsDiv as $comp):?>
				  				<div class="item text-center">
				  					<?php foreach($comp as $co):$imageArray = explode(',' , $co->imagenes);?>
				  						<div class="content-componente">
				  							<?php if(!empty($imageArray[0])): ?>
				  								<div class="img-comp" style="background-image:url('<?=base_url()?>/asset/img/uploads/<?=$imageArray[0]?>');"></div>
				  							<?php else: ?>
				  								<div class="img-comp" style="background-image:url('<?=base_url()?>/asset/img/img-componente-prdet.png');"></div>
				  							<?php endif; ?>
				  							<?php
				  								$in_session = "0";
				  								if(!empty($_SESSION["cart"])) {
				  								    if($_SESSION['cart'][$producto[0]->uniq]['pad_'.$co->uniq]) {
				  										$in_session = "1";
				  								    }
				  								}
				  							?>
				  							<div class="box-content-comp pads-select">
				  								<div class="custom-check">
				  									<label class="leabel-cust">
				  									  <input id="check_<?=$co->uniq?>" type="checkbox" <?php if($in_session == 1): echo "checked"; endif; ?>>
				  									  <span id="add_pad_<?=$co->uniq?>" class="checkmark" onClick = "cartAction('add','<?=$producto[0]->uniq?>','pad_<?=$co->uniq?>')" <?php if($in_session != "0") { ?>style="display:none" <?php } ?>></span>
				  									  <span id="added_pad_<?=$co->uniq?>" class="checkmark" onClick = "cartAction('remove','<?=$producto[0]->uniq?>','pad_<?=$co->uniq?>',)" <?php if($in_session != "1") { ?>style="display:none;" <?php } ?>></span>
				  									</label>
				  								</div>
				  								<div class="content-comp">
				  									<h3><?=$co->codigo?></h3>
				  									<p><?=$co->nombre?></p>
				  								</div>
				  							</div>
				  						</div>
				  					<?php endforeach; ?>
				  				</div>
				  			<?php endforeach; ?>
			  		</div>
			  		<div class="btn-absolutes">
				  		<a class="btn-modal" style="background:#001871;" href="<?=base_url().$this->config->item('language_abbr')?>/componentes?ver=<?=$_GET['ver']?>&procedmiento=<?=$_GET['procedmiento']?>&quiofano=<?=$_GET['quiofano']?>" class="link-comp"><?=$this->lang->line('lbl_componentes')?></a>
				  		<div class="btn-modal show-<?=$key?>"><?=$this->lang->line('lbl_posicion')?></div>
			  		</div>
			  	</div>
			  	<div class="modal-posicionamiento posicion-<?=$key?>">
			  		<div class="content-posicion">
			  			<div class="close-modal close-posicion-<?=$key?>">X</div>
			  			<h3><?=$posicion->posicion?><br><small><?=$procedimiento[0]->nombre?></small></h3>
		  			  	<img src="<?=base_url()?>asset/img/uploads/<?=$posicion->imagen?>" class="img-fluid"/>
			  		</div>
			  	</div>
			   <?php endforeach; ?>
			</div>
		</div>
	</div>
</section>
<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js?v=4'></script>
<script type="text/javascript">
	<?php foreach($posicionamientos_pads as $key => $posicion): ?>
		$(".show-<?=$key?>").click(function() { 
			$('.posicion-<?=$key?>').addClass('active-modal');
		});
		$(".close-posicion-<?=$key?>").click(function() { 
			$('.posicion-<?=$key?>').removeClass('active-modal');
		});
	<?php endforeach; ?>
	<?php foreach($posicionamientos_pads as $key => $posicion): ?>
		  	$('#componentes<?=$key?>').owlCarousel({
		  	  loop: false,
		  	  margin: 20,
		  	  nav: true,
		  	  autoplay: false,
		  	  autoplayHoverPause: false,
		  	  autoHeight: true,
		  	  responsive: {
		  	    0: {
		  	      items: 1,
	              autoHeight:true
		  	    },
		  	    600: {

		  	      items: 1,
	              autoHeight:true
		  	    },
		  	    1000: {
		  	      items: 5,
	              autoHeight:true
		  	    }
		  	  }
		  	});
    <?php endforeach; ?>
</script>