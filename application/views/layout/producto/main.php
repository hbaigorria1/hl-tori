<section class="producto-section" style="background: #f3f4fb">
	<div class="container d-flex flex-column h-100 w-100">
		<div class="d-block w-100" style="margin-bottom:auto">
			<h4>Detalles de la Solución</h4>
			<p><a href="<?=base_url().$this->config->item('language_abbr')?>/"><?=$this->lang->line('leng_home')?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>"><?=$this->lang->line('lbl_procedimientos')?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=4"><?=$procedimiento[0]->nombre?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/quirofanos?procedmiento=<?=$procedimiento[0]->uniq?>">Sala <?=$quirofano[0]->nombre?></a> > <?=$producto[0]->titulo?></p>
		</div>
		<section class="section intern-categoria internta-producto-ver <?php if(!empty($posicionamientos_producto)): ?>slide-no-bttn <?php endif; ?>">
			<div class="owl-carousel owl-theme" id="slideproduct">
				<!-- renderizar paginas -->
				<?=$html?>
			</div>
			<div class="col-12 text-center">
				<?php if(!empty($posicionamientos_producto)): ?>
					<a href="<?=base_url().$this->config->item('language_abbr')?>/componentes?ver=<?=$_GET['ver']?>&procedmiento=<?=$_GET['procedmiento']?>&quiofano=<?=$_GET['quiofano']?>" class="link-comp"><?=$this->lang->line('lbl_componentes')?></a>
					<?php if(!empty($posicionamientos_pads)): ?>
						<a href="<?=base_url().$this->config->item('language_abbr')?>/pads?ver=<?=$_GET['ver']?>&procedmiento=<?=$_GET['procedmiento']?>&quiofano=<?=$_GET['quiofano']?>" class="link-comp" style="background:#5369e3;"><?=$this->lang->line('lbl_pads')?></a>
					<?php endif; ?>
				<?php endif; ?>
				<form id="frmCart">
					<?php
						$in_session = "0";
						if(!empty($_SESSION["cart"])) {
						    if($_SESSION['cart'][$producto[0]->uniq]['uniq']) {
								$in_session = "1";
						    }
						}
					?>
					<input type="button" id="add_uniq" value="<?=$this->lang->line('lbl_solicitar_mas_info')?>" class="btnAddAction cart-action" onClick = "cartAction('add','<?=$producto[0]->uniq?>','uniq')" <?php if($in_session != "0") { ?>style="display:none" <?php } ?>/>
					<input type="button" id="added_uniq" value="Agregado" class="btnAdded" <?php if($in_session != "1") { ?>style="display:none;" <?php } ?> />
				</form>
			</div>
			
		</section>
	</div>
</section>