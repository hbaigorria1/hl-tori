<section class="home-section" style="background: url('<?php echo base_url() ?>asset/img/back-tipo-ciru.jpg')">
	<div>
		<h4><?=$this->lang->line('lbl_tipos_quirofanos')?></h4>
		<p><?=$this->lang->line('lbl_tipos_quirofanos_desc')?></p>
	</div>
    <div class="btns-home w-100 d-flex align-items-center justify-content-center flex-wrap">
    	<?php foreach($quirofanos as $qui): ?>
			<a href="<?=base_url().$this->config->item('language_abbr')?>/solucion?procedmiento=<?=$_GET['procedmiento']?>&quiofano=<?=$qui->uniq?>" class="botton-home" style="background:<?=$qui->color?>;">
				<h3><?=$qui->nombre?></h3>
			</a>
		<?php endforeach; ?>
    </div>
</section>
<div class="tori-robot">
	<div class="box-content-robot">
		<div class="close-roboto-box">X</div>
		<p id="intro"></p>
	</div>
	<img src="<?=base_url()?>asset/img/tori-icon.png" class="img-fluid max-w-img-tori">
	<div class="number">1</div>
</div>