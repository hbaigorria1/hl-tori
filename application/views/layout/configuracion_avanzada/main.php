<section class="home-section" style="background: url('<?php echo base_url() ?>asset/img/back-config.jpg')">
	<div>
		<h4><?=$this->lang->line('lbl_tlt_config')?></h4>
	</div>
    <div class="btns-home w-100 d-flex align-items-center justify-content-center flex-wrap">
    	<form id="msform" action="<?=base_url().$this->config->item('language_abbr')?>/producto" method="get" class="w-100 d-flex align-items-center justify-content-center flex-wrap">
    		<p>
    	<div class="custom-select">
    	  <select name="procedmiento" required>
    	    <option value=""><?=$this->lang->line('lbl_config_avanzada_01')?>:</option>
    	    <?php foreach($procedimientos as $sub): ?>
    	    	<option value="<?=$sub->uniq?>"><?=$sub->nombre?></option>
    		<?php endforeach; ?>
    	  </select>
    	</div>
    	<div class="custom-select">
    	  <select name="quirofano" required>
    	    <option value=""><?=$this->lang->line('lbl_config_avanzada_02')?>:</option>
    	    <?php foreach($quirofanos as $qui): ?>
    	    	<option value="<?=$qui->uniq?>"><?=$qui->nombre?></option>
    		<?php endforeach; ?>
    	  </select>
    	</div>
    	<div class="custom-select">
    	  <select name="ver" required>
    	    <option value=""><?=$this->lang->line('lbl_config_avanzada_03')?>:</option>
    	    <?php foreach($productos as $prod): ?>
    	    	<option value="<?=$prod->uniq?>"><?=$prod->titulo?></option>
    		<?php endforeach; ?>
    	  </select>
    	</div>
    	<div class="custom-select">
    	 	<input type="submit" value="<?=$this->lang->line('lbl_config_avanzada_04')?>">
    	</div>
    	</form>
    </div>
</section>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js'></script>
<script>

	    $("#msform").validate(
	      {
	        rules: 
	        {
	          procedmiento: 
	          {
	            required: true
	          },
	          quirofano: 
	          {
	            required: true
	          },
	          ver: 
	          {
	            required: true
	          }
	        },
	        messages: 
	        {
	          procedmiento: 
	          {
	            required: "<?=$this->lang->line('lbl_config_avanzada_01')?>"
	          },
	          quirofano: 
	          {
	            required: "<?=$this->lang->line('lbl_config_avanzada_02')?>"
	          },
	          ver: 
	          {
	            required: "<?=$this->lang->line('lbl_config_avanzada_03')?>"
	          }
	        }
	      });	
</script>