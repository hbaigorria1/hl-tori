<section class="componente-section w-100" style="background: #f3f4fb">
	<div class="container d-flex flex-column h-100">
		<div class="d-block w-100" style="margin-bottom:0">
			<h4><?=$this->lang->line('lbl_tlt_compon')?></h4>
			<p><a href="<?=base_url().$this->config->item('language_abbr')?>/"><?=$this->lang->line('leng_home')?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=4"><?=$this->lang->line('lbl_procedimientos')?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=4"><?=$procedimiento[0]->nombre?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/quirofanos?procedmiento=<?=$procedimiento[0]->uniq?>">Sala <?=$quirofano[0]->nombre?></a> > <a href="<?=base_url().$this->config->item('language_abbr')?>/producto?ver=<?=$producto[0]->uniq?>&procedmiento=<?=$procedimiento[0]->uniq?>&quiofano=<?=$quirofano[0]->uniq?>"><?=$producto[0]->titulo?></a> > <?=$this->lang->line('lbl_componentes_min')?></p>
		</div>
		
		<div class="content-componentes" style="margin-bottom:auto;height:70%;">
			<div class="pn-ProductNav_Wrapper">
			<nav id="pnProductNav" class="pn-ProductNav">
			    <div id="pnProductNavContents" class="pn-ProductNav_Contents" id="" role="tablist">
			        <?php foreach($posicionamientos_producto as $key => $posicion): ?>
						    <a class="pn-ProductNav_Link nav-link <?php
						    if(isset($_GET['posicion'])):if($_GET['posicion'] == $posicion->uniq): echo "active"; endif;else:if($key == 0): echo "active"; endif; endif; ?>" id="pos<?=$posicion->uniq?>-tab" data-toggle="pill" href="#pos<?=$posicion->uniq?>" role="tab" aria-controls="pos<?=$posicion->uniq?>" <?php
						    if(isset($_GET['posicion'])):if($_GET['posicion'] == $posicion->uniq): echo "aria-selected='true'"; endif;else:if($key == 0): echo "aria-selected='true'"; endif; endif; ?> ><?=$posicion->posicion?></a>
					
				   <?php endforeach; ?>
				   <?php foreach($opcionales as $key => $opci): ?>
				   	<a class="pn-ProductNav_Link nav-link" id="opc<?=$opci->uniq?>-tab" data-toggle="pill" href="#opc<?=$opci->uniq?>" role="tab" aria-controls="opc<?=$opci->uniq?>"><?=$opci->posicion?></a>
				   <?php endforeach; ?>
				<span id="pnIndicator" class="pn-ProductNav_Indicator"></span>
			    </div>
			</nav>
				<button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
					<svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"/></svg>
				</button>
				<button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
					<svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024"><path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"/></svg>
				</button>
			</div>
			<?php if(false): ?>
			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				<?php foreach($posicionamientos_producto as $key => $posicion): ?>
					  <li class="nav-item">
					    <a class="nav-link <?php
					    if(isset($_GET['posicion'])):if($_GET['posicion'] == $posicion->uniq): echo "active"; endif;else:if($key == 0): echo "active"; endif; endif; ?>" id="pos<?=$posicion->uniq?>-tab" data-toggle="pill" href="#pos<?=$posicion->uniq?>" role="tab" aria-controls="pos<?=$posicion->uniq?>" aria-selected="true"><?=$posicion->posicion?></a>
					  </li>
				
			   <?php endforeach; ?>
			   <?php foreach($opcionales as $key => $opci): ?>
			   	<li class="nav-item">
			   	  <a class="nav-link" id="opc<?=$posicion->uniq?>-tab" data-toggle="pill" href="#opc<?=$opci->uniq?>" role="tab" aria-controls="opc<?=$opci->uniq?>" aria-selected="true"><?=$opci->posicion?></a>
			   	</li>
			   <?php endforeach; ?>
			</ul>
			<?php endif; ?>
			<div class="tab-content h-100" id="pills-tabContent">
			   <?php foreach($posicionamientos_producto as $key => $posicion): ?>
			  	<div class="tab-pane p-relative fade h-100 <?php
					    if(isset($_GET['posicion'])):if($_GET['posicion'] == $posicion->uniq): echo "show active"; endif;else:if($key == 0): echo "show active"; endif; endif; ?>" id="pos<?=$posicion->uniq?>" role="tabpanel" aria-labelledby="pos<?=$posicion->uniq?>-tab">
			  		<div id="componentes<?=$posicion->uniq?>" class="owl-carousel owl-theme slider-products h-100" >
			  			<?php $componentesProductos = $this->page_model->get_componentes_producto($posicion->uniq,$producto[0]->uniq, $language);
			  			$componentesDiv = array_chunk($componentesProductos, 2);?>
				  			<?php foreach($componentesDiv as $comp):?>
				  				<div class="item text-center">
				  					<?php foreach($comp as $co):$imageArray = explode(',' , $co->imagenes);?>
				  						<div class="content-componente">
				  							<div class="tag">
				  								<?=$this->lang->line('lbl_cantidad')?> <span><b><?=$co->cantidad?></b></span>
				  							</div>
				  							<?php if(!empty($imageArray[0])): ?>
				  								<div class="img-comp" style="background-image:url('<?=base_url()?>/asset/img/uploads/<?=$imageArray[0]?>');"></div>
				  							<?php else: ?>
				  								<div class="img-comp" style="background-image:url('<?=base_url()?>/asset/img/img-componente-prdet.png');"></div>
				  							<?php endif; ?>
				  							<div class="box-content-comp">
				  								<div class="custom-check">
				  									<label class="leabel-cust">
				  									  <input type="checkbox">
				  									  <span class="checkmark"></span>
				  									</label>
				  								</div>
				  								<div class="content-comp">
				  									<h3><?=$co->codigo?></h3>
				  									<p><?=$co->componente?></p>
				  								</div>
				  							</div>
				  						</div>
				  					<?php endforeach; ?>
				  				</div>
				  			<?php endforeach; ?>
			  		</div>
			  		<div class="btn-absolutes">
			  			<?php if(!empty($posicionamientos_pads)): ?>
			  				<a class="btn-modal" href="<?=base_url().$this->config->item('language_abbr')?>/pads?ver=<?=$_GET['ver']?>&procedmiento=<?=$_GET['procedmiento']?>&quiofano=<?=$_GET['quiofano']?>" style="background:#5369e3;"><?=$this->lang->line('lbl_pads')?></a>
			  			<?php endif; ?>
				  		<form id="frmCart">
				  			<?php
				  				$in_session = "0";
				  				if(!empty($_SESSION["cart"])) {
				  				    if($_SESSION['cart'][$producto[0]->uniq][$posicion->uniq]) {
				  						$in_session = "1";
				  				    }
				  				}
				  			?>
				  			<input type="button" id="add_<?=$posicion->uniq?>" value="<?=$this->lang->line('lbl_solicitar_mas_info')?>" class="btnAddAction cart-action" onClick = "cartAction('add','<?=$producto[0]->uniq?>','<?=$posicion->uniq?>')" <?php if($in_session != "0") { ?>style="display:none" <?php } ?>/>
				  			<input type="button" id="added_<?php echo $posicion->uniq; ?>" value="Agregado" class="btnAdded" <?php if($in_session != "1") { ?>style="display:none;" <?php } ?> />
				  		</form>
				  		<div class="btn-modal show-<?=$posicion->uniq?>"><?=$this->lang->line('lbl_posicion')?></div>
			  		</div>
			  	</div>
			  	<div class="modal-posicionamiento posicion-<?=$posicion->uniq?>">
			  		<div class="content-posicion">
			  			<div class="close-modal close-posicion-<?=$posicion->uniq?>">X</div>
			  			<h3><?=$posicion->posicion?><br><small><?=$procedimiento[0]->nombre?></small></h3>
			  			<div class="image-posicion" style="background:url('<?=base_url()?>asset/img/uploads/<?=$posicion->imagen?>');"></div>
			  		</div>
			  	</div>
			   <?php endforeach; ?>
			   <?php foreach($opcionales as $key => $opci): ?>
			  	<div class="tab-pane p-relative fade h-100" id="opc<?=$opci->uniq?>" role="tabpanel" aria-labelledby="opc<?=$opci->uniq?>-tab">
			  		<div id="opcionales<?=$opci->uniq?>" class="owl-carousel owl-theme slider-products h-100" >
			  			<?php $opcionalesProductos = $this->page_model->get_opcionales_producto($opci->uniq,$producto[0]->uniq, $language);
			  			$opcionalesDiv = array_chunk($opcionalesProductos, 2);?>
				  			<?php foreach($opcionalesDiv as $opcdiv):?>
				  				<div class="item text-center">
				  					<?php foreach($opcdiv as $opvalu):$imageArray = explode(',' , $opvalu->imagenes);?>
				  						<div class="content-componente">
				  							<div class="tag">
				  								<?=$this->lang->line('lbl_cantidad')?> <span><b><?=$opvalu->cantidad?></b></span>
				  							</div>
				  							<?php if(!empty($imageArray[0])): ?>
				  								<div class="img-comp" style="background-image:url('<?=base_url()?>/asset/img/uploads/<?=$imageArray[0]?>');"></div>
				  							<?php else: ?>
				  								<div class="img-comp" style="background-image:url('<?=base_url()?>/asset/img/img-componente-prdet.png');"></div>
				  							<?php endif; ?>
				  							<?php
				  								$in_session_op = "0";
				  								if(!empty($_SESSION["cart"])) {
				  								    if($_SESSION['cart'][$producto[0]->uniq]['op_'.$opvalu->uniq]) {
				  										$in_session_op = "1";
				  								    }
				  								}
				  							?>
				  							<div class="box-content-comp opc-select">
				  								<div class="custom-check">
				  									<label class="leabel-cust">
				  									  <input type="checkbox" <?php if($in_session_op == 1): echo "checked"; endif; ?>>
				  									  <span id="add_op_<?=$opvalu->uniq?>" class="checkmark" onClick = "cartAction('add','<?=$producto[0]->uniq?>','op_<?=$opvalu->uniq?>')" <?php if($in_session_op != "0") { ?>style="display:none" <?php } ?>></span>
				  									  <span id="added_op_<?=$opvalu->uniq?>" class="checkmark" onClick = "cartAction('remove','<?=$producto[0]->uniq?>','op_<?=$opvalu->uniq?>',)" <?php if($in_session_op != "1") { ?>style="display:none;" <?php } ?>></span>
				  									</label>
				  								</div>
				  								<div class="content-comp">
				  									<h3><?=$opvalu->codigo?></h3>
				  									<p><?=$opvalu->componente?></p>
				  								</div>
				  							</div>
				  						</div>
				  					<?php endforeach; ?>
				  				</div>
				  			<?php endforeach; ?>
			  		</div>
			  		<div class="btn-absolutes">
			  			<?php if(!empty($posicionamientos_pads)): ?>
			  				<a class="btn-modal" href="<?=base_url().$this->config->item('language_abbr')?>/pads?ver=<?=$_GET['ver']?>&procedmiento=<?=$_GET['procedmiento']?>&quiofano=<?=$_GET['quiofano']?>" style="background:#5369e3;"><?=$this->lang->line('lbl_pads')?></a>
			  			<?php endif; ?>
				  		<div class="btn-modal show-<?=$opci->uniq?>"><?=$this->lang->line('lbl_posicion')?></div>
			  		</div>
			  	</div>
	  		  	<div class="modal-posicionamiento posicion-<?=$opci->uniq?>">
	  		  		<div class="content-posicion">
	  		  			<div class="close-modal close-posicion-<?=$opci->uniq?>">X</div>
	  		  			<h3><?=$opci->posicion?><br><small><?=$procedimiento[0]->nombre?></small></h3>
	  		  			<div class="image-posicion" style="background:url('<?=base_url()?>asset/img/uploads/<?=$opci->imagen?>');"></div>
	  		  		</div>
	  		  	</div>
			   <?php endforeach; ?>
			</div>
		</div>
	</div>
</section>

<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script>
<script type="text/javascript">
	<?php foreach($posicionamientos_producto as $key => $posicion): ?>
		$(".show-<?=$posicion->uniq?>").click(function() { 
			$('.posicion-<?=$posicion->uniq?>').addClass('active-modal');
		});
		$(".close-posicion-<?=$posicion->uniq?>").click(function() { 
			$('.posicion-<?=$posicion->uniq?>').removeClass('active-modal');
		});
	<?php endforeach; ?>

	<?php foreach($opcionales as $key => $opci): ?>
		$(".show-<?=$opci->uniq?>").click(function() { 
			$('.posicion-<?=$opci->uniq?>').addClass('active-modal');
		});
		$(".close-posicion-<?=$opci->uniq?>").click(function() { 
			$('.posicion-<?=$opci->uniq?>').removeClass('active-modal');
		});
	<?php endforeach; ?>
	<?php foreach($posicionamientos_producto as $key => $posicion): ?>
		  	$('#componentes<?=$posicion->uniq?>').owlCarousel({
		  	  loop: false,
		  	  margin: 10,
		  	  nav: true,
		  	  autoplay: false,
		  	  autoplayHoverPause: false,
		  	  autoHeight: true,
		  	  responsive: {
		  	    0: {
		  	      items: 1,
	              autoHeight:true
		  	    },
		  	    600: {

		  	      items: 1,
	              autoHeight:true
		  	    },
		  	    1000: {
		  	      items: 5,
	              autoHeight:true
		  	    }
		  	  }
		  	});
    <?php endforeach; ?>

    <?php foreach($opcionales as $key => $opc): ?>
		  	$('#opcionales<?=$opc->uniq?>').owlCarousel({
		  	  loop: false,
		  	  margin: 10,
		  	  nav: true,
		  	  autoplay: false,
		  	  autoplayHoverPause: false,
		  	  autoHeight: true,
		  	  responsive: {
		  	    0: {
		  	      items: 1,
	              autoHeight:true
		  	    },
		  	    600: {

		  	      items: 1,
	              autoHeight:true
		  	    },
		  	    1000: {
		  	      items: 5,
	              autoHeight:true
		  	    }
		  	  }
		  	});
    <?php endforeach; ?>
</script>

<script type="text/javascript">
	var SETTINGS = {
	    navBarTravelling: false,
	    navBarTravelDirection: "",
	   navBarTravelDistance: 150
	}

	document.documentElement.classList.remove("no-js");
	document.documentElement.classList.add("js");

	// Out advancer buttons
	var pnAdvancerLeft = document.getElementById("pnAdvancerLeft");
	var pnAdvancerRight = document.getElementById("pnAdvancerRight");
	// the indicator
	var pnIndicator = document.getElementById("pnIndicator");

	var pnProductNav = document.getElementById("pnProductNav");
	var pnProductNavContents = document.getElementById("pnProductNavContents");

	pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));

	// Set the indicator
	moveIndicator(pnProductNav.querySelector("[aria-selected=\"true\"]"));

	// Handle the scroll of the horizontal container
	var last_known_scroll_position = 0;
	var ticking = false;

	function doSomething(scroll_pos) {
	    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
	}

	pnProductNav.addEventListener("scroll", function() {
	    last_known_scroll_position = window.scrollY;
	    if (!ticking) {
	        window.requestAnimationFrame(function() {
	            doSomething(last_known_scroll_position);
	            ticking = false;
	        });
	    }
	    ticking = true;
	});


	pnAdvancerLeft.addEventListener("click", function() {
	  // If in the middle of a move return
	    if (SETTINGS.navBarTravelling === true) {
	        return;
	    }
	    // If we have content overflowing both sides or on the left
	    if (determineOverflow(pnProductNavContents, pnProductNav) === "left" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
	        // Find how far this panel has been scrolled
	        var availableScrollLeft = pnProductNav.scrollLeft;
	        // If the space available is less than two lots of our desired distance, just move the whole amount
	        // otherwise, move by the amount in the settings
	        if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
	            pnProductNavContents.style.transform = "translateX(" + availableScrollLeft + "px)";
	        } else {
	            pnProductNavContents.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
	        }
	        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
	        pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
	        // Update our settings
	        SETTINGS.navBarTravelDirection = "left";
	        SETTINGS.navBarTravelling = true;
	    }
	    // Now update the attribute in the DOM
	    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
	});

	pnAdvancerRight.addEventListener("click", function() {
	    // If in the middle of a move return
	    if (SETTINGS.navBarTravelling === true) {
	        return;
	    }
	    // If we have content overflowing both sides or on the right
	    if (determineOverflow(pnProductNavContents, pnProductNav) === "right" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
	        // Get the right edge of the container and content
	        var navBarRightEdge = pnProductNavContents.getBoundingClientRect().right;
	        var navBarScrollerRightEdge = pnProductNav.getBoundingClientRect().right;
	        // Now we know how much space we have available to scroll
	        var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
	        // If the space available is less than two lots of our desired distance, just move the whole amount
	        // otherwise, move by the amount in the settings
	        if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
	            pnProductNavContents.style.transform = "translateX(-" + availableScrollRight + "px)";
	        } else {
	            pnProductNavContents.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
	        }
	        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
	        pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
	        // Update our settings
	        SETTINGS.navBarTravelDirection = "right";
	        SETTINGS.navBarTravelling = true;
	    }
	    // Now update the attribute in the DOM
	    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
	});

	pnProductNavContents.addEventListener(
	    "transitionend",
	    function() {
	        // get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
	        var styleOfTransform = window.getComputedStyle(pnProductNavContents, null);
	        var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
	        // If there is no transition we want to default to 0 and not null
	        var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
	        pnProductNavContents.style.transform = "none";
	        pnProductNavContents.classList.add("pn-ProductNav_Contents-no-transition");
	        // Now lets set the scroll position
	        if (SETTINGS.navBarTravelDirection === "left") {
	            pnProductNav.scrollLeft = pnProductNav.scrollLeft - amount;
	        } else {
	            pnProductNav.scrollLeft = pnProductNav.scrollLeft + amount;
	        }
	        SETTINGS.navBarTravelling = false;
	    },
	    false
	);

	// Handle setting the currently active link
	pnProductNavContents.addEventListener("click", function(e) {
	  var links = [].slice.call(document.querySelectorAll(".pn-ProductNav_Link"));
	  links.forEach(function(item) {
	    item.setAttribute("aria-selected", "fal22se");
	    $(item).removeClass("active");
	  })
	  e.target.setAttribute("aria-selected", "true");
	  moveIndicator(e.target);
	});

	function moveIndicator(item, color) {
	    var textPosition = item.getBoundingClientRect();
	    var container = pnProductNavContents.getBoundingClientRect().left;
	    var distance = textPosition.left - container;
	    pnIndicator.style.transform = "translateX(" + (distance + pnProductNavContents.scrollLeft) + "px) scaleX(" + textPosition.width * 0.01 + ")";
	    if (color) {
	        pnIndicator.style.backgroundColor = colour;
	    }
	}

	function determineOverflow(content, container) {
	    var containerMetrics = container.getBoundingClientRect();
	    var containerMetricsRight = Math.floor(containerMetrics.right);
	    var containerMetricsLeft = Math.floor(containerMetrics.left);
	    var contentMetrics = content.getBoundingClientRect();
	    var contentMetricsRight = Math.floor(contentMetrics.right);
	    var contentMetricsLeft = Math.floor(contentMetrics.left);
	   if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
	        return "both";
	    } else if (contentMetricsLeft < containerMetricsLeft) {
	        return "left";
	    } else if (contentMetricsRight > containerMetricsRight) {
	        return "right";
	    } else {
	        return "none";
	    }
	}

</script>