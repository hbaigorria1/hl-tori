$(".registro-comercio").submit(function(event){
	event.preventDefault(); //prevent default action 
	var post_url = $(this).attr("action"); //get form action url
	var request_method = $(this).attr("method"); //get form GET/POST method
	var form_data = $(this).serialize(); //Encode form elements for submission
	
	$.ajax({
		url : post_url,
		type: request_method,
		data : form_data,
		beforeSend: function() {
	        // setting a timeout
	        $('.enviar-btn-2').addClass('disable');
	        $('.gif-load-2').show('slow');
	    },
	    success: function(response){
	        alert(response);
	        //window.location.replace(response);
	    }
	});
});