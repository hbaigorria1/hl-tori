<div class="nav col-sm-2">
	
	<div class="title-nav">LENGUAJE</div>
	<a href="<?php echo base_url() ?>countries/"><div class="btn-nav" id="l_countries">Idiomas >> </div></a>
		
	<div class="title-nav">SELECCIÓN</div>
		<a href="<?php echo base_url() ?>config/"><div class="btn-nav" id="l_config">Home >> </div></a>
		<a href="<?php echo base_url() ?>categorias/"><div class="btn-nav" id="l_categorias">Tipo de seleccion >> </div></a>
	<div class="title-nav">PRODUCTOS</div>
		<a href="<?php echo base_url() ?>productos/"><div class="btn-nav" id="l_productos">Productos >> </div></a>
	<div class="title-nav">COMPONENTES</div>
		<a href="<?php echo base_url() ?>componentes/"><div class="btn-nav" id="l_componentes">Componentes >> </div></a>
		<a href="<?php echo base_url() ?>posicionamientos/"><div class="btn-nav" id="l_posicionamientos">Posicionamientos de componentes >> </div></a>
	<div class="title-nav">PADS</div>
		<a href="<?php echo base_url() ?>pads/"><div class="btn-nav" id="l_pads">Pads >> </div></a>
	<div class="title-nav">OPCIONALES</div>
		<a href="<?php echo base_url() ?>opcionales/"><div class="btn-nav" id="l_opcionales">Opcionales >> </div></a>
	<div class="title-nav">QUIROFANOS</div>
		<a href="<?php echo base_url() ?>quirofanos/"><div class="btn-nav" id="l_categorias">Quirofanos >> </div></a>
	<div class="title-nav">ADMINISTRADOR</div>
	    <a href="<?php echo base_url() ?>productos_modulos/"><div class="btn-nav" id="l_productos_modulos">M&oacute;dulos >> </div></a>	
	
	<?php if($this->session->userdata['logged_in']['administrator']==1){ ?>
	<div class="title-nav">USUARIOS</div>
	<a href="<?php echo base_url() ?>usuarios/"><div class="btn-nav" id="l_usuarios">Administradores >> </div></a>	
	<?php } ?>
</div>