<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:10px;">
		<div class="listado">
			<div class="col-md-12 home-tools">
				<div class="row">
					<div class="col-xs-8 col-md-8">
						<h2>MODULOS DE EMAILS</h2>
					</div>
					<div class="col-xs-4 col-md-4">
						<a href="<?php echo base_url()?>productos_modulos/add/"><div class="btn btn-success btn-sm bt-save pull-right" style="margin-right:8px;">AGREGAR NUEVO</div></a>
					</div>
				</div>
			</div>
			<table id="list" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th style="display:none">Id</th>
						<th>M&oacute;dulo</th>
						<th>Variables</th>
						<th>Fecha modificaci&oacute;n</th>
						<th width="40">Editar</th>
						<th width="40">Eliminar</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$html='';
						foreach ( $info as $fila )
						{
							
							
							$variables = $this->page_model->get_modulo_variables($fila->{'id'});
							$variables_string = "";
							foreach($variables as $v)
							{
								$variables_string.= "%".$v->code."%<br>";
							}
						
						   if($fila->{"user_name"} != "admin"):
						
							$html.='<tr>
								<td style="display:none;">'.$fila->{'id'}.'</td>
								<td><b>'.$fila->{'modulo'}.'</b><br><img style="max-width:100px" src="'.base_url().'asset/img/uploads/'.$fila->image.'"></td>
								<td><b>'.$variables_string.'</b></td>
								<td>'.date("d/m/Y H:i:s",$fila->{'modified_at'}).'</td>
								<td align="center"><a href="'.base_url().'productos_modulos/edit/'.$fila->{'id'}.'/"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
								<td align="center"><a href="#" data-href="'.base_url().'productos_modulos/remove/'.$fila->{'id'}.'/" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
							</tr>';
							
							endif;
						}
						echo $html;
					?>				
				</tbody>
			</table>
		</div>
	</div>
</div>
<br style="clear:both;"/>