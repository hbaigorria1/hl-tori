<div class="listado" style="padding:0;margin-top:0;">
	<div class="col-md-12 home-tools">
		<div class="row">
			<div class="col-md-8">
				<h2>VARIABLES</h2>
			</div>
			<div class="col-md-4">
				<div class="btn btn-success btn-sm  pull-right" data-toggle="modal" data-target="#exampleModalLong" style="margin-right:8px;">AGREGAR NUEVA VARIABLE</div>
			</div>
		</div>
	</div>
	<table id="list" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th>Varibale</th>
				<th>Code</th>
				<th width="40">Editar</th>
				<th width="40">Eliminar</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$html='';
			foreach ( $variables_module as $fila ){
					$html.='<tr>
						<td>'.$fila->{'variable'}.'</td>
						<td>'.$fila->{'code'}.'</td>
						<td align="center"><a href="#" data-toggle="modal" data-target="#editarchivo'.$fila->{'id'}.'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
						<td align="center"><a href="#" data-href="'.base_url().'productos_modulos/delete_variable/'.$fila->{'id'}.'/?id='.$_GET['id'].'" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
					</tr>';?>
          <?php $get_module_variable = $this->page_model->get_modulo_variables_id($fila->{'id'}); ?>
          <!-- Modal -->
          <div class="modal fade" id="editarchivo<?php echo $fila->{'id'}?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h3 class="modal-title" id="exampleModalLongTitle" style="display:inline-block;">Editar Variable</h3>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="form-edit" method="POST" action="<?php echo base_url() ?>productos_modulos/update_variable/<?php echo $get_module_variable[0]->{'id'}?>/?id=<?php echo $_GET['id']?>">
                    <input type="hidden" name="id_archivo" value="<?php echo $get_module_variable[0]->{'id'} ?>">
                    <div class="td-input">
                      <h5 style="margin-top:0;"><b>Variable</b></h5>
                      <input type="text" name="variable" placeholder="Variable" value="<?php echo $get_module_variable[0]->{'variable'} ?>">
                    </div>
                    <div class="td-input">
                      <h5><b>Code</b></h5>
                      <input type="text" name="code" placeholder="Code" value="<?php echo $get_module_variable[0]->{'code'} ?>">
                    </div>
                    
                    <div class="text-right">
                      <input type="submit" class="btn btn-success btn-sm" value="Guardar">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
			
			<?php  }
			echo $html;
		?>				
		</tbody>
	</table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle" style="display:inline-block;">Agregar Archivo</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-save" method="POST" action="<?php echo base_url() ?>productos_modulos/insert_variable/?id=<?php echo $_GET['id'] ?>/">
        	<div class="td-input">
	        	<h5 style="margin-top:0;"><b>Variable</b></h5>
	        	<input type="text" name="variable" placeholder="Variable">
        	</div>
          <div class="td-input">
            <h5 style="margin-top:0;"><b>Code</b></h5>
            <input type="text" name="code" placeholder="Code">
          </div>
        	<div class="text-right">
        		<input type="submit" class="btn btn-success btn-sm" value="Guardar">
        	</div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery-1.11.1.js"></script>
<script type="text/javascript">
      $(".form-save").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission

        var iframe = document.getElementById('archivos');
        
        
        $.ajax({
          url : post_url,
          type: request_method,
          data : form_data,
          beforeSend: function() {
                // setting a timeout
                $('.enviar').addClass('disable');
                $('.gif').show('slow');
                $('.message').hide("slow");
            },
            success: function(response){
              if(response == 'none'){
                window.location.replace("<?php echo base_url() ?>productos_modulos/vista_variables/");
              } else {
                window.location.replace("<?php echo base_url() ?>productos_modulos/vista_variables/?id="+response+"/");
              }
            }
        });
      });

      $(".form-edit").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission

        var iframe = document.getElementById('archivos');
        
        
        $.ajax({
          url : post_url,
          type: request_method,
          data : form_data,
          beforeSend: function() {
                // setting a timeout
                $('.enviar').addClass('disable');
                $('.gif').show('slow');
                $('.message').hide("slow");
            },
            success: function(response){
              if(response == 'none'){
                window.location.replace("<?php echo base_url() ?>productos_modulos/vista_variables/");
              } else {
                window.location.replace("<?php echo base_url() ?>productos_modulos/vista_variables/?id="+response+"/");
              }
            }
        });
      });
</script>