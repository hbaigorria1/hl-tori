<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>

<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">	EDITAR RUBRO</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
			 <form method="post" action="<?php echo base_url()?>tipos/update/<?php echo $info[0]->{'uniq'}?>/">
			 	<!-- Nav tabs -->
			 	<ul class="nav nav-tabs">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<li class="nav-item">
			 			  <a class="nav-link <?php if($keylang == 0): ?> active <?php endif; ?>" data-toggle="tab" href="#sec<?php echo $lan->{'id'} ?>">Titulo <?php echo $lan->{'titulo'} ?></a>
			 			</li>
			 		<?php endforeach; ?>
			 	</ul>
			 	<!-- Tab panes -->
			 	<div class="tab-content">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<div class="tab-pane lang-buttons <?php if($keylang == 0): ?> active <?php endif; ?>" id="sec<?php echo $lan->{'id'} ?>">
				 			
			 				<div class="td-input">
								<b>Nombre:</b><br>
								<input type="text" name="nombre_<?php echo $lan->{'abr'} ?>" id="nombre" value="<?php if(isset($info[$keylang]->{'nombre'})) echo $info[$keylang]->{'nombre'} ?>">
							</div>

			 				<div class="td-input">
								<b>Color:</b><br>
								<input type="text" name="color_<?php echo $lan->{'abr'} ?>" id="color" value="<?php if(isset($info[$keylang]->{'color'})) echo $info[$keylang]->{'color'} ?>">
							</div>
							
			 				<div class="td-input">
								<b>Descripci&oacute;n:</b><br>
								<input type="text" name="descripcion_<?php echo $lan->{'abr'} ?>" id="descripcion" value="<?php if(isset($info[$keylang]->{'descripcion'})) echo $info[$keylang]->{'descripcion'} ?>">
							</div>
				 			
					        <div class="td-input">
<!--		    					<input type="text" name="title_<?php echo $lan->{'abr'} ?>" id="title" placeholder="Titulo <?php echo $lan->{'titulo'} ?>" <?php if($info[$keylang]->{'lang'} == $lan->{'id'}): ?> value="<?php echo $info[$keylang]->{'nombre'} ?>" <?php endif; ?>>-->
		    					<input type="hidden" name="id_idioma_<?php echo $lan->{'abr'} ?>" value="<?php echo $lan->{'id'} ?>">
		    					<!--<input type="hidden" name="id_columna" value="<?php echo $info[$keylang]->{'id'} ?>">-->
		    				</div>

			 			</div>
			 		<?php endforeach; ?>
     
                
			 		</div>
			 	</div>
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>categorias/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>




