<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>

<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Configurar modulo</a></li>
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
			 <form method="post" action="<?php echo base_url()?>productos_blocks/update_config_blocks/1/">
			 	<ul class="nav nav-tabs">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<li class="nav-item">
			 			  <a class="nav-link" <?php if($_GET['lang'] == $lan->id): ?> style="background:#5369E3;" <?php endif; ?> href="<?=base_url()?>productos_blocks/configure_block/<?=$this->session->userdata('block_id')?>/?lang=<?=$lan->id?>">Titulo <?php echo $lan->{'titulo'} ?></a>
			 			</li>
			 		<?php endforeach; ?>
			 	</ul>
			 	
			 	<!-- Tab panes -->
			 	<div class="tab-content">
				 	<input type="hidden" name="id_idioma" value="<?=$_GET['lang']?>">
				 	<?php foreach($variables as $v): ?>
				 		<?php if($v->tipo == "image"): ?>
				 			<div class="td-input">
				 				<b><?=$v->variable?>:</b><br>
				 				<input type="text" name="galeria<?=$v->id?>_input" id="galeria<?=$v->id?>_input" class="img-input" value="<?php echo $this->page_model->get_value_module_id($_GET['lang'],$this->session->userdata('block_id'),$v->id);?>" readonly>
				 				<div id="main_uploader">
				 					<div class="uploader-id<?=$v->id?>">
				 						<div id="uploader<?=$v->id?>" align="left">
				 							<input id="uploadify<?=$v->id?>" type="file" class="uploader" />
				 						</div>
				 					</div>
				 					<div id="filesUploaded" style="display:none;"></div>
				 					<div id="thumbHeight<?=$v->id?>" style="display:none;" >800</div>
				 					<div id="thumbWidth<?=$v->id?>" style="display:none;" >1440</div>
				 				</div>
				 				<div id="galeria<?=$v->id?>" class="upload-galeria">
				 					<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $this->page_model->get_value_module_id($_GET['lang'],$this->session->userdata('block_id'),$v->id);?>',function(){}); $('#field_<?=$v->id?>').val(''); $(this).parent().remove();"></div><img src="../../../asset/img/uploads/<?php echo $this->page_model->get_value_module_id($_GET['lang'],$this->session->userdata('block_id'),$v->id);?>" width="auto" height="auto" style="max-width:100%"><br><input id="img_desc" type="text"></div>
				 				</div>
				 			</div>
				 		<?php elseif($v->tipo == "textline"): ?>
							<div class="td-input">
								<b><?=$v->variable?>:</b><br>
								<input type="text" name="field_<?=$v->id?>" id="field_<?=$v->id?>" value="<?php echo $this->page_model->get_value_module_id($_GET['lang'],$this->session->userdata('block_id'),$v->id);?>">
								</div>
						<?php elseif($v->tipo == "textarea"): ?>
							<div class="td-input">
								<b><?=$v->variable?>:</b><br>
								<textarea name="field_<?=$v->id?>_<?php echo $lan->{'abr'} ?>" id="field_<?=$v->id?>">
									<?php echo $this->page_model->get_value_module_id($_GET['lang'],$this->session->userdata('block_id'),$v->id);?>
								</textarea>
							</div>
						<?php elseif($v->tipo == "posicion"): ?>
							<div class="td-input">
								<b><?=$v->variable?>:</b><br>
								<select name="field_<?=$v->id?>_<?php echo $lan->{'abr'} ?>" id="field_<?=$v->id?>">
									<?php foreach($posicionamientos as $posicion): ?>
										<option value="<?=$posicion->uniq?>" <?php if($posicion->uniq == $this->page_model->get_value_module_id($_GET['lang'],$this->session->userdata('block_id'),$v->id)): echo "selected"; endif; ?>><?=$posicion->nombre?></option>
									<?php endforeach; ?>
								</select>
							</div>
				 		<?php endif; ?>
					<?php endforeach; ?>
				</div>
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>productos_modulos/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>

<script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js"></script> 
<script type="text/javascript" src="<?php echo base_url() ?>asset/js/advanced.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.uploadifive.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	/*UPLOAD CONFIG*/
	var w=1440;
	var h=810;
	var path='../../../asset/img/uploads/';
	
	
	var maxWidth=1440;
	var thWidth=1440;
	var thHeight=810;		
	var tipo='unica';
	var allowedTypes='jpg,png,gif,mp4'
	var callback=function(){console.log('upload complete');}

	<?php foreach($variables as $v): ?>
		<?php if($v->tipo == "image"): ?>
			uploaderNoCrop('<?=$v->id?>',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
		<?php endif; ?>
	<?php endforeach; ?>
	});
</script>