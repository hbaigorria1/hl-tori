<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_componentes extends CI_Controller {
	 
	function __construct()
	{
       parent::__construct();
       
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('form');
	   $this->load->helper('url');
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/mailing_blocks.js?v='.time().'');
	    
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador - Pauny', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();

		// getting email_id from uri
		$email_id = $this->uri->segment(3);
		
		// $data["mailing_info"] = $this->page_model->get_mailing_id($email_id);
						
		// fixing order cuando no tiene nada.
		$this->page_model->fix_orden_nulo($email_id);
				
		
		// save email_id into session
		$this->session->set_userdata('email_id', $email_id);

		$info =  $this->page_model->get_producto_componentes($this->uri->segment(3));
		$data['info'] = $info;
		
		//el contenido de nuestro formulario estar� en views/registro/formulario_registro,
		//de esta forma tambi�n podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/productos_componentes/list', $data, TRUE); 
	    
		//la secci�n footer ser� el archivo views/registro/footer_template
	    //$this->template->write_view('footer', 'layout/footer');   
	    
		//con el m�todo render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}


	public function add(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');
		
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
		
		$data["lang"] =  $this->page_model->get_countries();
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$data['posicionamientos'] =  $this->page_model->get_posicionamientos();
		$data['componentes'] =  $this->page_model->get_componentes();
		$this->template->write_view('content', 'layout/productos_componentes/add', $data, TRUE); 
		$this->template->render();
	}
        
	// --
	// Update mailing
	// --

	public function save(){
		if (isset($this->session->userdata['logged_in'])) {
			$data = array(
				'id_componente' => $_POST['id_componente'],
				'id_posicionamiento' => $_POST['id_posicionamiento'],
				'id_producto' => $_POST['id_producto'],
				'cantidad' => $_POST['cantidad'],
			);
			$this->page_model->insert_producto_componente($data);
			redirect('productos_componentes/index/'.$_POST['id_producto'].'/');
		}else{
			redirect('login/');
		}
		
	}
	public function update(){
		if (isset($this->session->userdata['logged_in'])) {
			$data = array(
				'id_componente' => $_POST['id_componente'],
				'id_posicionamiento' => $_POST['id_posicionamiento'],
				'cantidad' => $_POST['cantidad'],
			);
			$this->page_model->update_producto_componente($data);
			redirect('productos_componentes/index/'.$_POST['id_producto'].'/');
		}else{
			redirect('login/');
		}
	}
	
	public function remove(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_producto_componente();
			redirect('productos_componentes/index/'.$_GET['uniq'].'/');
		}else{
			redirect('login/');
		}
	}
	
	public function edit(){
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/usuarios.js');
		
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador - Pauny', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$info =  $this->page_model->get_producto_componente_id($this->uri->segment(3), $_GET['uniq']);		
		$data['info']=$info;
		$data['posicionamientos'] =  $this->page_model->get_posicionamientos();
		$data['componentes'] =  $this->page_model->get_componentes();
		$this->template->write_view('content', 'layout/productos_componentes/edit', $data, TRUE); 
		$this->template->render();
	}
	
	// Logout from admin page
	public function logout() {
		// Removing session data
		$sess_array = array(
		'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		redirect('home/');
	}
	
}
