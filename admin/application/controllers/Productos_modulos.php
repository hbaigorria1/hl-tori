<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_modulos extends CI_Controller
{
	function __construct()
	{
       parent::__construct();
       
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('form');
	   $this->load->helper('url');
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/usuarios.js');
	    
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador - Pauny', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();

		$info =  $this->page_model->get_modulos();
		$data['info']=$info;
		
		//el contenido de nuestro formulario estar� en views/registro/formulario_registro,
		//de esta forma tambi�n podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/productos_modulos/list', $data, TRUE); 
	    
		//la secci�n footer ser� el archivo views/registro/footer_template
	    //$this->template->write_view('footer', 'layout/footer');   
	    
		//con el m�todo render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}


	public function add(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js');
		
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		
		$data = '';
		
		$this->template->write_view('content', 'layout/productos_modulos/add', $data, TRUE); 
		$this->template->render();
	}

	public function vista_variables(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js');
	
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		
		$data['variables_module'] = $this->page_model->get_modulo_variables($_GET['id']);
		
		$this->template->write_view('content', 'layout/productos_modulos/vista_variables', $data, TRUE); 
		$this->template->render();
	}
	
	public function save(){
		if (isset($this->session->userdata['logged_in'])) {
			$data = array(
				'modulo' => $_POST['modulo'],
				'html' => $_POST['html'],
				'image' => $_POST['galeria1_input']
			);
			$this->db->insert('modulos', $data);
			$id_modulo = $this->db->insert_id();
			redirect('productos_modulos/edit/'.$id_modulo.'/');
		}
		
	}

	public function insert_variable(){
		if (isset($this->session->userdata['logged_in'])) {
			$data = array(
				'modulo_id' => $_GET['id'],
				'variable' => $_POST['variable'],
				'tipo' =>'textline',
				'code' => $_POST['code'],
			);
			$this->db->insert('modulos_variables', $data);
			if($_GET['id'] != 0):
				echo $_GET['id'];
				exit;
			else:
				echo 'none';
				exit;
			endif;
		}
		
	}

	public function update_variable(){
		if (isset($this->session->userdata['logged_in'])) {
			$data = array(
				'variable' => $_POST['variable'],
				'code' => $_POST['code'],
			);
			$this->db->where('id', $this->uri->segment(3));
			$this->db->update('modulos_variables', $data);
			if($_GET['id'] != 0):
				echo $_GET['id'];
				exit;
			else:
				echo 'none';
				exit;
			endif;
		}
		
	}

	public function update(){
		if (isset($this->session->userdata['logged_in']))
		{
			$data = array(
				'modulo' => $_POST['modulo'],
				'html' => $_POST['html'],
				'image' => $_POST['galeria1_input']
			);

			$this->page_model->update_modulo($data);

			redirect('productos_modulos/');
		}else{
			redirect('login/');
		}
		
	}
	public function remove(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_modulo();
			redirect('productos_modulos/');
		}else{
			redirect('login/');
		}
	}

	public function delete_variable(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->delete('modulos_variables');
            //$this->page_model->remove_categories();
            if($_GET['id'] != 0):
            	redirect('productos_modulos/vista_variables/?id='.$_GET['id'].'/');
            else:
            	redirect('productos_modulos/vista_variables/');
            endif;
		}else{
			redirect('login/');
		}
	}
	
	public function edit(){
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js');
		
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$info =  $this->page_model->get_modulo_id($this->uri->segment(3));		
		$data['info']=$info;
		
		$this->template->write_view('content', 'layout/productos_modulos/edit', $data, TRUE); 
		$this->template->render();
	}
	
	// Logout from admin page
	public function logout() {
		// Removing session data
		$sess_array = array(
		'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		redirect('home/');
	}
	
	public function SendForm()
	{
		$parmsJSON = (isset($_POST['_p']))?$_POST['_p']:$_GET['_p'];
		$parmsJSON = urldecode(base64_decode ( $parmsJSON ));
		$JSON = new Services_JSON();
		$parmsJSON = $JSON->decode($parmsJSON);
		$asunto = $parmsJSON->{'asunto'};
		$mensaje = rawURLdecode($parmsJSON->{'mensaje'});
		$name = $parmsJSON->{'name'};
		$email = $parmsJSON->{'email'};
		$para = $parmsJSON->{'para'};
			
		$mAIL = new MAIL;
		$mAIL->From($email,$name);
        							
		$mAIL->AddTo($para);
		 $mAIL->Subject(utf8_encode($asunto));
									
	     $contact['message_body'] = $mensaje;
							        
		$mAIL->Html($contact['message_body']);
									
	 
		$cON = $mAIL->Connect("smtp.gmail.com", (int)465, "diego.mantovani@gmail.com", "p4t0f30p4t0f30", "tls") or die(print_r($mAIL->Result));
        $mAIL->Send($cON) ? $sent = true : $sent = false;
		$mAIL->Disconnect();
		
		
		if(!$sent) {
		 print '{"resultado":"NO","error":"'.$mAIL->Result.'"}';
		} else {
		  print '{"resultado":"OK"}';
		}

		exit;
			
	}
}
